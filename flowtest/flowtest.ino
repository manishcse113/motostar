byte sensorInterrupt = 0;  // 0 = digital pin 2
byte sensorPin       = 2;

// The hall-effect flow sensor outputs approximately 4.5 pulses per second per
// litre/minute of flow.
float calibrationFactor = 4.5;
volatile byte pulseCount;  
float flowRate;
unsigned int flowMilliLitres;
unsigned long totalMilliLitres;
unsigned long oldTime;

int MAXFlowReadings =  20;
int flowReadings[30] = {0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0,  0,  0,  0,  0,  0,  0,  0 };
int flowReadingCount = 0;
int flowReadingiteration = 0;

byte newWaterFlowState = HIGH;
byte waterInFlowState = HIGH;
bool isWaterInFlow = false;
unsigned long waterGoneTimeMillis = 0;
byte motorOnWithoutWaterMins = 0;

void setup()
{
  
  // Initialize a serial connection for reporting values to the host
  Serial.begin(9600); 
   
  pinMode(sensorPin, INPUT);
  digitalWrite(sensorPin, HIGH);
  pulseCount        = 0;
  flowRate          = 0.0;
  flowMilliLitres   = 0;
  totalMilliLitres  = 0;
  oldTime           = 0;

  // The Hall-effect sensor is connected to pin 2 which uses interrupt 0.
  // Configured to trigger on a FALLING state change (transition from HIGH
  // state to LOW state)
  attachInterrupt(sensorInterrupt, pulseCounter, FALLING);
}

/**
 * Main program loop
 */
void loop()
{
   calculateFlowSensor();
}


void calculateFlowSensor()
{
  
  if((millis() - oldTime) > 1000)    // Only process counters once per second
  {
    
    // Disable the interrupt while calculating flow rate and sending the value to
    // the host
    detachInterrupt(sensorInterrupt);

    flowReadings[flowReadingiteration] = pulseCount; //pulsecount is pulse count in that particular second
    flowReadingCount = flowReadingCount + pulseCount; //add the current reading
    //minus the reading which is current - last maxth reading... so to have sum of only last max-1 enteries.... 
    // It means, we have to minus the next to the current reading...so as to have sum of last max-1 enteries
    if (flowReadingiteration >= MAXFlowReadings-1)
    {
      flowReadingiteration=0;
    }
    else
    {
      flowReadingiteration++;
    }
    flowReadingCount = flowReadingCount - flowReadings[flowReadingiteration];    
    //Serial.print("Pulse Count: ");
    //Serial.print(pulseCount);
    //Serial.print(" -- ");
    //Serial.println(flowReadingCount);

    if(flowReadingCount > 0)
    {
      //Consider this as "Pani Aa raha hai"
      newWaterFlowState = LOW;
    }
    else
    {
      //Consider this as "Pani nahi aa raha hai"
      newWaterFlowState = HIGH;
    }
    if (newWaterFlowState ==   waterInFlowState) //No change in state - same as last one
    {
       //lastDebounceTimeFlow = 0;
       if (waterInFlowState == LOW) //connected
       {
          //Serial.print ("Paani aa raha hai");
          isWaterInFlow = true;
       }
       else //not connected
       {
          //Serial.print ("Paani nahi aa raha hai ");
          isWaterInFlow = false;  
       }
    }
    else
    {
          if (waterInFlowState == LOW) //It means previously water was on - now it is gone.
          {
            waterGoneTimeMillis = millis();
            motorOnWithoutWaterMins = 0;
            Serial.print ("Paani just abhee chala gaya ");
            isWaterInFlow = false;
            waterInFlowState = HIGH;
            //M digitalWrite(ledWaterFlow, LOW);
          }
          // only toggle the LED if the new button state is HIGH
          else  
          {
            Serial.print ("Paani just abhee aa gaya");
            isWaterInFlow = true;
            waterInFlowState = LOW;
            //M digitalWrite(ledWaterFlow, HIGH);
            //M save_water_detect_time();
            //M sendEventToDevice(eventId_Water_Detect_Time);
          }
    }
    
    // Because this loop may not complete in exactly 1 second intervals we calculate
    // the number of milliseconds that have passed since the last execution and use
    // that to scale the output. We also apply the calibrationFactor to scale the output
    // based on the number of pulses per second per units of measure (litres/minute in
    // this case) coming from the sensor.
    flowRate = ((1000.0 / (millis() - oldTime)) * pulseCount) / calibrationFactor;
    
    
    // Note the time this processing pass was executed. Note that because we've
    // disabled interrupts the millis() function won't actually be incrementing right
    // at this point, but it will still return the value it was set to just before
    // interrupts went away.
    oldTime = millis();
    
    // Divide the flow rate in litres/minute by 60 to determine how many litres have
    // passed through the sensor in this 1 second interval, then multiply by 1000 to
    // convert to millilitres.
    flowMilliLitres = (flowRate / 60) * 1000;
    
    // Add the millilitres passed in this second to the cumulative total
    totalMilliLitres += flowMilliLitres;
      
    unsigned int frac;
    /*
    // Print the flow rate for this second in litres / minute
    Serial.print("Flow rate: ");
    Serial.print(int(flowRate));  // Print the integer part of the variable
    Serial.print("L/min");
    Serial.print("\t");       // Print tab space

    // Print the cumulative total of litres flowed since starting
    Serial.print("Output Liquid Quantity: ");        
    Serial.print(totalMilliLitres);
    Serial.println("mL"); 
    Serial.print("\t");       // Print tab space
    Serial.print(totalMilliLitres/1000);
    Serial.print("L");
    */

    // Reset the pulse counter so we can start incrementing again
    pulseCount = 0;
    
    // Enable the interrupt again now that we've finished sending output
    attachInterrupt(sensorInterrupt, pulseCounter, FALLING);
  }  
}

/*
Insterrupt Service Routine
 */
void pulseCounter()
{
  // Increment the pulse counter
  pulseCount++;
}
