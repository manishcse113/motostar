#include "Wire.h" 
//#include <LiquidCrystal.h>

#include <SoftwareSerial.h>// import the serial library
#include <EEPROM.h>                                                                                           

#include <NewPing.h>


#define MOTOR_START_REASON_MANUAL_TRIGGER 1
#define MOTOR_START_REASON_APP_TRIGGER 2
#define MOTOR_START_REASON_WATER_FLOW_DETECTED 3
#define MOTOR_START_REASON_AUTO_TIME 4
#define MOTOR_STOP_REASON_MANUAL_TRIGGER 5
#define MOTOR_STOP_REASON_WATER_FLOW_LOST 6
#define MOTOR_STOP_REASON_TANK_FULL 7
#define MOTOR_STOP_REASON_APP_TRIGGER 8

SoftwareSerial Genotronex(0, 1); // RX, TX
byte ledpin=2; // led on D13 will show blink on / off
byte BluetoothData; // the data given from Computer


//int highLevelSensor = 11;    //The pin location of the sensor
//int lowLevelSensor = 0;    //The pin location of the sensor

/* For Nano
byte ledSystemOn = 5;
byte ledWaterFlow = 6;
byte ledMotorOn = 7;

byte ledTankLow = 8;
byte ledTankMid =9;
byte ledTankHigh = 10;
byte ledTankFull = 11;

byte lowLevelSensor = 25;    //The pin location of the sensor
byte midLevelSensor = 26;    //The pin location of the sensor
byte highLevelSensor = 17;    //The pin location of the sensor
byte tankFullSensor = 18;    //The pin location of the sensor
byte hallsensor = 16;    //The pin location of the sensor


#define TRIGGER_PIN  14
#define ECHO_PIN     15
*/
//Folllowing For Uno
byte ledSystemOn = 9;
byte ledWaterFlow = 3;
byte ledMotorOn = 4;

byte ledTankLow = 5;
byte ledTankMid =6;
byte ledTankHigh = 7;
byte ledTankFull = 8;

byte lowLevelSensor = 9;    //The pin location of the sensor
byte midLevelSensor = 10;    //The pin location of the sensor
byte highLevelSensor = 11;    //The pin location of the sensor
byte tankFullSensor = 12;    //The pin location of the sensor

byte hallsensor = 2;    //The pin location of the sensor

byte sensorInterrupt = 0;  // 0 = digital pin 2
byte sensorPin       = 2;

// The hall-effect flow sensor outputs approximately 4.5 pulses per second per
// litre/minute of flow.
float calibrationFactor = 4.5;
volatile byte pulseCount;  
float flowRate;
unsigned int flowMilliLitres;
unsigned long totalMilliLitres;
unsigned long oldTime;

int MAXFlowReadings =  20;
int flowReadings[30] = {0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 0, 0,  0,  0,  0,  0,  0,  0,  0 };
int flowReadingCount = 0;
int flowReadingiteration = 0;

byte newWaterFlowState = HIGH;

#define TRIGGER_PIN  11
#define ECHO_PIN     13

byte SirenOn = A0;
byte forceMotorStartSignal = A1;
byte motoMode = A2;
byte bulb = A3;



#define MAX_DISTANCE 200

// defines pins numbers
const int trigPin = TRIGGER_PIN;
const int echoPin = ECHO_PIN;
long cm, inches;

NewPing sonar(TRIGGER_PIN, ECHO_PIN, MAX_DISTANCE);



int duration = 0;

//byte scl=A5
//byte sda = A4

//int fullLed = 13;
//int motorSwitch = 2;

bool motorSwitchCondition = false;
int significantTimeMotorRunningWithoutWater = 0;
//int MAXITERATIONS_WITHOUT_WATER = 300; //It need to be calibrated

bool isTankFilled = true; //By default set to true - just as a heck. as otherwise -at first - it detects that water flow is there and tank is empty and hence starts the motor.
bool isWaterInFlow = false;
//bool isTankLevelLow = false;
boolean isTankLevelHigh = true;
boolean isTankMid = false;
boolean isTankLow = false;
boolean isTankLevelVeryLow = false;

byte lowLevelState = HIGH; // LOW means Connected
byte midLevelState = HIGH; // LOW means Connected
byte highLevelState = HIGH; // LOW means Connected
byte tankFullState = HIGH; // LOW means Connected
byte waterInFlowState = HIGH; //No Water

byte reading1;
byte reading2;
byte reading3;
byte reading4;
byte reading5;

// the following variables are unsigned long's because the time, measured in miliseconds,
// will quickly become a bigger number than can't be stored in an int.
unsigned long lastDebounceTimeTank = 0;  // the last time the output pin was toggled
unsigned long debounceDelayTank = 1000;    // the debounce time; increase if the output flickers

unsigned long lastTimeTankFull = 0;
unsigned long motorRestartGuardTimeAfterTankFull = 100000;  // At least 5 minutes  - motor shall not run automatically - after getting full status::


unsigned long lastDebounceTimeLevel1 = 0;  // the last time the output pin was toggled
unsigned long debounceDelayTankLevel1 = 1000;    // the debounce time; increase if the output flickers

unsigned long lastDebounceTimeLevel2 = 0;  // the last time the output pin was toggled
unsigned long debounceDelayTankLevel2 = 1000;    // the debounce time; increase if the output flickers

unsigned long lastDebounceTimeLevel3 = 0;  // the last time the output pin was toggled
unsigned long debounceDelayTankLevel3 = 1000;    // the debounce time; increase if the output flickers

unsigned long lastDebounceTimeFlow = 0;  // the last time the output pin was toggled
unsigned long debounceDelayFlow = 200;    // the debounce time; increase if the output flickers


unsigned long lastDebounceTimeManualMode = 0;  // the last time the manual mode was toggled
unsigned long debounceDelayManual = 2;    // the debounce time; increase if the output flickers
byte manualModeState = LOW; //means automatic mode;

//Following are Date/Time - Functions::
#define DS3231_I2C_ADDRESS 0x68 

//Data for customization
byte date = 15;
byte month = 10;
byte year = 16;
byte day = 7; //days are Sunday =1, Saturday =7
byte hour = 18;
byte minute = 46;
byte second = 0;


byte ODDHOURS[10] = {0, 1, 2, 3, 4, 13, 14, 15, 16, 23};
byte MAXODDHOURS = 10;

byte HOURS[12] = {5, 6,  6,   6,   6,  7,  19, 19,  19,  19, 20, 20};
byte MINS[12] = {45,  0,  15,  30,  45,  0,  0,  15,  30,  45,  00, 15 };

float levelDistances[12] = {0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0, 0 };
const byte MaxLevelIndexes = 12;
byte currentIndexlevelDistances = 0;

//byte morningStartHour = 5;
//byte morningStartMinute = 45;
//byte morningMaxRetry = 8;

//byte eveningStartHour = 19;
//byte eveningStartMinute = 0;
//byte eveningMaxRetry = 8;

//byte retryInterval = 15;



byte MAXITERATIONS = 12;
//byte MAXMINSWITHOUTWATER = 2;

byte current_time_in_minutes;
byte next_time_in_minutes;
byte next_new_time_in_minutes;
int  minutes_remaining ;
int new_minutes_remaining;

unsigned long waterGoneTimeMillis = 0;
byte motorOnWithoutWaterMins = 0;

char serialSendData[32];


boolean isSpeakerOn = false;
unsigned long speakerOnTimeMillis = 0;
int speakerOnMaxTimeSec = 10;


//Spearker settings
int speakerOut = A0;
byte names[] = {'c', 'd', 'e', 'f', 'g', 'a', 'b', 'C', 'c', 'd', 'e', 'f', 'g', 'a', 'b', 'C'                       };  
int tones[] = {1915, 1700, 1519, 1432, 1275, 1136, 1014, 956, 1915, 1700, 1519, 1432, 1275, 1136, 1014, 956     };
byte melody[] = "2d2a1f2c2d2a2d2c2f2d2a2c2d2a1f2c2d2a2a2g2p8p8p8p2d2a1f2c2d2a2d2c2f2d2a2c2d2a1f2c2d2a2a2g2p8p8p8p";


//byte names[] = {'c', 'd', 'e', 'f', 'g', 'a', 'b', 'C'                       };  
//int tones[] = {1915, 1700, 1519, 1432, 1275, 1136, 1014, 956     };
//byte melody[] = "2d2a1f2c2d2a2d2c2f2d2a2c2d2a1f2c2d2a2a2g2p8p8p8p2d";

// count length: 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0
//                                10                  20                  30
byte count = 0;
byte count2 = 0;
byte count3 = 0;
byte MAX_COUNT = 50;
byte statePin = LOW;



// TONES  ==========================================
// Start by defining the relationship between 
//       note, period, &  frequency. 
#define  c     3830    // 261 Hz 
#define  d     3400    // 294 Hz 
#define  e     3038    // 329 Hz 
#define  f     2864    // 349 Hz 
#define  g     2550    // 392 Hz 
#define  a     2272    // 440 Hz 
#define  b     2028    // 493 Hz 
#define  C     1912    // 523 Hz 
// Define a special note, 'R', to represent a rest
#define  R     0
byte DEBUG = 1;
int melody_off[] = {  C,  b,  g,  C,  b,   e, C,  b,  g,  C };
int beats_off[]  = { 16, 16, 16,  8,  8,  16, 16, 16, 16,  8}; 

//int melody_off[] = {  C,  b,  g,  C,  b,   e };
//byte beats_off[]  = { 16, 16, 16,  8,  8,  16}; 

//byte MAX_COUNT_OFF = sizeof(melody_off) / 2; // Melody length, for looping.
byte MAX_COUNT_OFF = 1; // Melody length, for looping.

int melody_level[] = {  b };
byte beats_level[]  = { 8}; 
byte MAX_COUNT_LEVELCHANGE = 1;//sizeof(melody_level) / 2; // Melody length, for looping.

// Set overall tempo
long tempo = 80000;
// Set length of pause between notes
int pause = 1000;
// Loop variable to increase Rest length
byte rest_count = 100; //<-BLETCHEROUS HACK; See NOTES

// Initialize core variables
int tone_ = 0;
int beat = 0;





// PLAY TONE  ==============================================
// Pulse the speaker to play a tone for a particular duration
void playTone() {
  long elapsed_time = 0;
  if (tone_ > 0) { // if this isn't a Rest beat, while the tone has 
    //  played less long than 'duration', pulse speaker HIGH and LOW
    while (elapsed_time < duration) {

      digitalWrite(speakerOut,HIGH);
      delayMicroseconds(tone_ / 2);

      // DOWN
      digitalWrite(speakerOut, LOW);
      delayMicroseconds(tone_ / 2);

      // Keep track of how long we pulsed
      elapsed_time += (tone_);
    } 
  }
  else { // Rest beat; loop times delay
    for (int j = 0; j < rest_count; j++) { // See NOTE on rest_count
      delayMicroseconds(duration);  
    }                                
  }                                 
}



//EEPROM functions - read/write
//EEPROM Structure::

int EEPROM_POS_VERSION = 0;
byte sw_version =1;    //ver :0
byte sw_sub_version =0;  //subver :1




byte eventId_Motor_Start_Time = 0;
byte eventId_Motor_Stop_Time = 1;
byte eventId_Auto_Start_Time = 2;
byte eventId_Auto_Stop_Time = 3;
byte eventId_Water_Detect_Time = 4;
byte eventId_Water_Lost_Time = 5;
byte eventId_Full_Detect_Time = 6;
byte eventId_Full_Lost_Time = 7;
byte eventId_High_Detect_Time = 8;
byte eventId_High_Lost_Time = 9;
byte eventId_Mid_Detect_Time = 10;
byte eventId_Mid_Lost_Time = 11;
byte eventId_Low_Detect_Time = 12;
byte eventId_Low_Lost_Time = 13;

byte eventId_Mode_Manual_Time = 14;
byte eventId_Mode_Auto_Time = 15;

byte EEPROM_CONFIGURED = 51;

int EEPROM_POS_PROFILE = 2;
byte eeprom_configured = 0;  //profile_value : 2
byte mor_start_hr = 5;  //morning start hr: 3
byte mor_start_min = 45; //morning start min: 4
byte mor_max_retry = 8; //morningmaxretry : 5
byte eve_start_hr = 17;  //evening start hr: 6
byte eve_start_min = 35;   //evening start min : 7
byte eve_max_retry = 10;   //evening max retry : 8
byte retry_interval = 2;  //retryInterval : 9
byte MAXMINSWITHOUTWATER = 2;  //MAXMINSWITHOUTWATER : 10



boolean is_eeprom_configured()
{
    int pos = EEPROM_POS_PROFILE;
    eeprom_configured = EEPROM.read(pos);  //morning start hr
    //return false;

    if (eeprom_configured == EEPROM_CONFIGURED)
    {
        return true;
    }
    else
    {
        return false;
    }
}

bool is_Time_to_Run_Motor(byte current_hr, byte current_min, byte second)
{
  int current_total_min = current_hr * 60 + current_min;

  int mor_start_total_min = mor_start_hr * 60 + mor_start_min;
  int eve_start_total_min = eve_start_hr * 60 + eve_start_min;

  boolean time_to_run = false;

  //Serial.print("Morning Start Time is-->");
  //Serial.print(mor_start_hr);   
  //Serial.print(":");
  //Serial.print(mor_start_min);
   
  //if (second ==0)
  { 
    for (int i =0; i < mor_max_retry ; i++)
    {
      mor_start_total_min = mor_start_total_min + i*retry_interval;
      if ( current_total_min == mor_start_total_min)
      {
         time_to_run = true;
      }   
    }
  
    for (int i =0; i < eve_max_retry ; i++)
    {
      eve_start_total_min = eve_start_total_min + i*retry_interval;
      if ( current_total_min == eve_start_total_min)
      {
         time_to_run = true;
      }   
    }
  }
  //Serial.print("Time to run Motor --> ");
  //Serial.print(time_to_run);
  return time_to_run;
}


void change_motor_profile()
{
  eeprom_configured = EEPROM_CONFIGURED;
  save_motor_profile();
}

void save_motor_profile()
{
    int pos = EEPROM_POS_PROFILE;
    EEPROM.update(pos, eeprom_configured);  //morning start hr
    pos++;
    EEPROM.update(pos, mor_start_hr);  //morning start hr
    pos++;
    EEPROM.update(pos, mor_start_min); //morning start min: 3
    pos++;
    EEPROM.update(pos, mor_max_retry); //morningmaxretry : 4
    pos++;
    EEPROM.update(pos, eve_start_hr);  //evening start hr: 5
    pos++;
    EEPROM.update(pos, eve_start_min);   //evening start min : 6
    pos++;
    EEPROM.update(pos, eve_max_retry);   //evening max retry : 7
    pos++;
    EEPROM.update(pos, retry_interval);  //retryInterval : 8
    pos++;
    EEPROM.update(pos, MAXMINSWITHOUTWATER);  //MAXMINSWITHOUTWATER : 9

/*
    Serial.print("Saving New Profile as Morning ");
    Serial.print(mor_start_hr);
    Serial.print(":");
    Serial.print(mor_start_min);
    Serial.print(":");
    Serial.print(mor_max_retry);
    
    Serial.print(" Evening ");
    Serial.print(eve_start_hr);
    Serial.print(":");
    Serial.print(eve_start_min);
    Serial.print(":");
    Serial.print(eve_max_retry);
    Serial.print("  DryMinute");
    Serial.print(MAXMINSWITHOUTWATER);
    Serial.print("\r\n");
*/ 
}


void get_motor_profile()
{
    int pos = EEPROM_POS_PROFILE;
    eeprom_configured = EEPROM.read(pos);  //morning start hr
    pos++;
    mor_start_hr = EEPROM.read(pos);  //morning start hr
    //mor_start_hr = mor_start_hr + (char)('0');
    pos++;
    mor_start_min = EEPROM.read(pos); //morning start min: 3
    //mor_start_min = mor_start_min  + (char)('0');
    pos++;
    mor_max_retry = EEPROM.read(pos); //morningmaxretry : 4
    //mor_max_retry = mor_max_retry + (char)('0');
    pos++;
    eve_start_hr = EEPROM.read(pos);  //evening start hr: 5
    //eve_start_hr = eve_start_hr + (char)('0');
    pos++;
    eve_start_min = EEPROM.read(pos);   //evening start min : 6
    //eve_start_min = eve_start_min + (char)('0');
    pos++;
    eve_max_retry = EEPROM.read(pos);   //evening max retry : 7
    //eve_max_retry = eve_max_retry + (char)('0');
    pos++;
    retry_interval = EEPROM.read(pos);  //retryInterval : 8
    //retry_interval = retry_interval + (char)('0');

    pos++;
    MAXMINSWITHOUTWATER = EEPROM.read(pos);  //MAXMINSWITHOUTWATER : 9
    
}


void save_version()
{
    int pos = EEPROM_POS_VERSION;
    EEPROM.update(pos, sw_version);  //morning start hr
    pos++;
    EEPROM.update(pos, sw_sub_version); //morning start min: 3
}


void get_version()
{
    int pos = EEPROM_POS_VERSION;
    sw_version = EEPROM.read(pos);  //morning start hr
    pos++;
    sw_sub_version = EEPROM.read(pos); //morning start min: 3
}



//Stats - position her is useless - as we are not going to save it in EEPROM - due to limitation of EEPROM read/write oprations - 100000 : keeping it only in SRAM
int EEPROM_POS_MOTOR_START_TIME = 11;
byte l_motor_start_date=0;  //Last Motor Start Date : 11
byte l_motor_start_month=0; //Last Motor Start Month : 12
byte l_motor_start_year=0;  //Last Motor Start Year : 13
byte l_motor_start_hr=0;     //Last Motor Start Hr: 14
byte l_motor_start_min=0;   //Last motor start min: 15 

 
byte l_motor_start_reason=0;  //Last Motor Start Trigger reason : 16

int EEPROM_POS_MOTOR_STOP_TIME = 17;
byte l_motor_stop_date=0;   //Last Motor Stop Date : 17
byte l_motor_stop_month=0;  //Last Motor Stop Month : 18
byte l_motor_stop_year=0;   //Last Motor Stop Year : 19
byte l_motor_stop_hr=0;     //Last Motor Stop Hr: 20
byte l_motor_stop_min=0;   //Last motor Stop min: 21 

byte l_motor_stop_reason=0;   //Last Motor Stop Trigger reason : 22

int EEPROM_POS_AUTO_START_TIME = 23;
byte l_auto_start_date=0;   //Last Auto Motor Start Date : 23
byte l_auto_start_month=0;   //Last Auto Motor Start Month : 24
byte l_auto_start_year=0;   //Last Auto Motor Start Year : 25
byte l_auto_start_hr=0;     //Last Auto Trigger Start Hr: 26
byte l_auto_start_min=0;    //Last Auto Trigger Start Min: 27


int EEPROM_POS_AUTO_STOP_TIME = 28;
byte l_auto_stop_date=0;   //Last Auto Motor Start Date : 28
byte l_auto_stop_month=0;   //Last Auto Motor Start Month : 29
byte l_auto_stop_year=0;   //Last Auto Motor Start Year : 30
byte l_auto_stop_hr=0;     //Last Auto Trigger Start Hr: 31
byte l_auto_stop_min=0;    //Last Auto Trigger Start Min: 32

int EEPROM_POS_WATER_DETECT_TIME = 33;
byte l_water_detect_date=0;   //Last Water Detected Start Date : 33
byte l_water_detect_month=0;   //Last Water Detected Start Month : 34
byte l_water_detect_year=0;   //Last Water Detected Start Year : 35
byte l_water_detect_hr=0;     //Last Water Detected Start Hr:36
byte l_water_detect_min=0;    //LAst Water Detected Start Min: 37

int EEPROM_POS_WATER_LOST_TIME = 38;
byte l_water_lost_date=0;   //Last Water Lost Date : 38
byte l_water_lost_month=0;    //Last Water Lost Month : 39
byte l_water_lost_year=0;   //Last Water Lost Year : 40
byte l_water_lost_hr=0;     //Last Water Lost Hr: 41
byte l_water_lost_min=0;     //LAst Water Lost Min: 42

int EEPROM_POS_FULL_DETECT_TIME = 43;
byte l_tank_full_detect_date=0;   //Last Tank Full Detected Date : 43
byte l_tank_full_detect_month=0;  //Last Tank Full Detected Month : 44
byte l_tank_full_detect_year=0;   //Last Tank Full Detected Year : 45
byte l_tank_full_detect_hr=0;   //Last Tank Full Detected Hr: 46
byte l_tank_full_detect_min=0;    //LAst Tank Full Detected Min: 47


int EEPROM_POS_FULL_LOST_TIME = 48;
byte l_tank_full_lost_date=0;   //Last Tank Full Lost Date : 48
byte l_tank_full_lost_month=0;  //Last Tank Full Lost Month : 49
byte l_tank_full_lost_year=0;   //Last Tank Full Lost Year : 50
byte l_tank_full_lost_hr=0;   //Last Tank Full Lost Hr: 51
byte l_tank_full_lost_min=0;  //LAst Tank Full Lost Min: 52

int EEPROM_POS_HIGH_DETECT_TIME = 53;
byte l_tank_high_detect_date=0;   //Last Tank High Detected Date : 53
byte l_tank_high_detect_month=0;  //Last Tank High Detected Month : 54
byte l_tank_high_detect_year=0;   //Last Tank High Detected Year : 55
byte l_tank_high_detect_hr=0;   //Last Tank High Detected Hr: 56
byte l_tank_high_detect_min=0;    //LAst Tank High Detected Min: 57

int EEPROM_POS_HIGH_LOST_TIME = 58;
byte l_tank_high_lost_date=0;   //Last Tank High Lost Date : 58
byte l_tank_high_lost_month=0;  //Last Tank High Lost Month : 59
byte l_tank_high_lost_year=0;   //Last Tank High Lost Year : 60
byte l_tank_high_lost_hr=0;   //Last Tank High Lost Hr: 61
byte l_tank_high_lost_min=0;    //LAst Tank High Lost Min: 62

int EEPROM_POS_MID_DETECT_TIME = 63;
byte l_tank_mid_detect_date=0;   //Last Tank Low Sensor Detected Date : 63
byte l_tank_mid_detect_month=0; //Last Tank Low Sensor Detected Month : 64
byte l_tank_mid_detect_year=0;  //Last Tank Low Sensor Detected Year : 65
byte l_tank_mid_detect_hr=0;  //Last Tank Low Sensor Detected Hr: 66
byte l_tank_mid_detect_min=0;   //LAst Tank Low Sensor Detected Min: 67

int  EEPROM_POS_MID_LOST_TIME = 68;
byte l_tank_mid_lost_date=0;    //Last Tank Low Sensor Lost Date : 68
byte l_tank_mid_lost_month=0;  //Last Tank Low Sensor Lost Month : 69
byte l_tank_mid_lost_year=0;  //Last Tank Low Sensor Lost Year : 70
byte l_tank_mid_lost_hr=0;  //Last Tank Low Sensor Lost Hr: 71
byte l_tank_mid_lost_min=0;   //LAst Tank Low Sensor Lost Min: 72

int EEPROM_POS_LOW_DETECT_TIME = 73;
byte l_tank_low_detect_date=0;   //Last Tank Low Sensor Detected Date : 73
byte l_tank_low_detect_month=0; //Last Tank Low Sensor Detected Month : 74
byte l_tank_low_detect_year=0;  //Last Tank Low Sensor Detected Year : 75
byte l_tank_low_detect_hr=0;  //Last Tank Low Sensor Detected Hr: 76
byte l_tank_low_detect_min=0;   //LAst Tank Low Sensor Detected Min: 77

int  EEPROM_POS_LOW_LOST_TIME = 78;
byte l_tank_low_lost_date=0;    //Last Tank Low Sensor Lost Date : 78
byte l_tank_low_lost_month=0;  //Last Tank Low Sensor Lost Month : 79
byte l_tank_low_lost_year=0;  //Last Tank Low Sensor Lost Year : 80
byte l_tank_low_lost_hr=0;  //Last Tank Low Sensor Lost Hr: 81
byte l_tank_low_lost_min=0;   //LAst Tank Low Sensor Lost Min: 82


int  EEPROM_POS_MODE_MANUAL_TIME = 83;
byte l_mode_manual_date=0;    //Last Tank Low Sensor Lost Date : 83
byte l_mode_manual_month=0;  //Last Tank Low Sensor Lost Month : 84
byte l_mode_manual_year=0;  //Last Tank Low Sensor Lost Year : 85
byte l_mode_manual_hr=0;  //Last Tank Low Sensor Lost Hr: 86
byte l_mode_manual_min=0;   //LAst Tank Low Sensor Lost Min: 87

int  EEPROM_POS_MODE_AUTO_TIME = 88;
byte l_mode_auto_date=0;    //Last Tank Low Sensor Lost Date : 88
byte l_mode_auto_month=0;  //Last Tank Low Sensor Lost Month : 89
byte l_mode_auto_year=0;  //Last Tank Low Sensor Lost Year : 90
byte l_mode_auto_hr=0;  //Last Tank Low Sensor Lost Hr: 91
byte l_mode_auto_min=0;   //LAst Tank Low Sensor Lost Min: 92


char serialStatsSendData[100];

char serialEventsSendData[100];

void sendtoDevice()
{
    encodeStatus();
    Serial.println(serialSendData);
}

void sendtoDevicePeriodic()
{
    encodeStatus();
    Serial.println(serialSendData);
}

void sendLastEventsToDevice()
{
    encodeAllLastEventsData();
    Serial.println(serialStatsSendData);
}

void sendEventToDevice(byte eventId)
{
    encodeEventData(eventId);
    Serial.println(serialEventsSendData);
}
void encodeEventData(byte eventId)
{
    int i =0;
    int ievent = eventId + '0';
    serialEventsSendData[i++] = 'a';
    serialEventsSendData[i++] = 'b';
    serialEventsSendData[i++] = 'e';

    if (eventId == eventId_Motor_Start_Time)
    {
       i += encodeEventIdMotorStart( & serialEventsSendData[i]);
    }
    else if (eventId == eventId_Motor_Stop_Time)
    {
       i += encodeEventIdMotorStop( & serialEventsSendData[i]);
    }
    else if (eventId == eventId_Auto_Start_Time)
    {
       i += encodeEventIdAutoStart( & serialEventsSendData[i]);
    }
    else if (eventId == eventId_Auto_Stop_Time)
    {
       i += encodeEventIdAutoStop( & serialEventsSendData[i]);
    }
    else if (eventId == eventId_Water_Detect_Time)
    {
       i += encodeEventIdWaterDetect( & serialEventsSendData[i]);
    }
    else if (eventId == eventId_Water_Lost_Time)
    {
       i += encodeEventIdWaterLost( & serialEventsSendData[i]);
    }
    else if (eventId == eventId_Full_Detect_Time)
    {
       i += encodeEventIdFullDetect( & serialEventsSendData[i]);
    }
    else if (eventId == eventId_Full_Lost_Time)
    {
       i += encodeEventIdFullLost( & serialEventsSendData[i]);
    }
    else if (eventId == eventId_High_Detect_Time)
    {
       i += encodeEventIdHighDetect( & serialEventsSendData[i]);
    }
    else if (eventId == eventId_High_Lost_Time)
    {
       i += encodeEventIdHighLost( & serialEventsSendData[i]);
    }
    else if (eventId == eventId_Mid_Detect_Time)
    {
       i += encodeEventIdMidDetect( & serialEventsSendData[i]);
    }
    else if (eventId == eventId_Mid_Lost_Time)
    {
       i += encodeEventIdMidLost( & serialEventsSendData[i]);
    }

    else if (eventId == eventId_Low_Detect_Time)
    {
       i += encodeEventIdLowDetect( & serialEventsSendData[i]);
    }
    else if (eventId == eventId_Low_Lost_Time)
    {
       i += encodeEventIdLowLost( & serialEventsSendData[i]);
    }
    
    else if (eventId == eventId_Mode_Manual_Time)
    {
       i += encodeEventIdManual( & serialEventsSendData[i]);
    }
    else if (eventId == eventId_Mode_Auto_Time)
    {
       i += encodeEventIdAuto( & serialEventsSendData[i]);
    }
     serialEventsSendData[i] = '\n';

}

void encodeAllLastEventsData()
{
    int i =0;
    serialStatsSendData[i++] = 'a';
    serialStatsSendData[i++] = 'b';
    serialStatsSendData[i++] = 'd';
   i += encodeEventIdMotorStart( & serialStatsSendData[i]);
   i += encodeEventIdMotorStop( & serialStatsSendData[i]);
   i += encodeEventIdAutoStart( & serialStatsSendData[i]);
   i += encodeEventIdAutoStop( & serialStatsSendData[i]);
   i += encodeEventIdWaterDetect( & serialStatsSendData[i]);
   i += encodeEventIdWaterLost( & serialStatsSendData[i]);
   i += encodeEventIdFullDetect( & serialStatsSendData[i]);
   i += encodeEventIdFullLost( & serialStatsSendData[i]);
   i += encodeEventIdHighDetect( & serialStatsSendData[i]);
   i += encodeEventIdHighLost( & serialStatsSendData[i]);
   i += encodeEventIdMidDetect( & serialStatsSendData[i]);
   i += encodeEventIdMidLost( & serialStatsSendData[i]);
   i += encodeEventIdLowDetect( & serialStatsSendData[i]);
   i += encodeEventIdLowLost( & serialStatsSendData[i]);
   
   i += encodeEventIdManual( & serialStatsSendData[i]);
   i += encodeEventIdAuto( & serialStatsSendData[i]);
   serialStatsSendData[i++] = '\n';
}

int encodeEventIdMotorStart(char *eventData)
{
    int i =0;
    eventData[i++] = eventId_Motor_Start_Time + '0';
    eventData[i++] = l_motor_start_date + '0';
    eventData[i++] = l_motor_start_month  + '0';
    eventData[i++] = l_motor_start_year + '0';
    eventData[i++] = l_motor_start_hr + '0';
    eventData[i++] = l_motor_start_min + '0';  
    eventData[i++] = l_motor_start_reason + '0';
    return i;
}

int encodeEventIdMotorStop(char *eventData)
{
    int i =0;
    eventData[i++] = eventId_Motor_Stop_Time + '0';
    eventData[i++] = l_motor_stop_date + '0';
    eventData[i++] = l_motor_stop_month  + '0';
    eventData[i++] = l_motor_stop_year + '0';
    eventData[i++] = l_motor_stop_hr + '0';
    eventData[i++] = l_motor_stop_min + '0';
    eventData[i++] = l_motor_stop_reason + '0';
    return i;
}

int encodeEventIdAutoStart(char *eventData)
{
    int i =0;
    eventData[i++] = eventId_Auto_Start_Time + '0';
    
    eventData[i++] = l_auto_start_date + '0';
    eventData[i++] = l_auto_start_month  + '0';
    eventData[i++] = l_auto_start_year + '0';
    eventData[i++] = l_auto_start_hr + '0';
    eventData[i++] = l_auto_start_min + '0';
    return i;
}

int encodeEventIdAutoStop(char *eventData)
{
    int i =0;
    eventData[i++] = eventId_Auto_Stop_Time + '0';
    eventData[i++] = l_auto_stop_date + '0';
    eventData[i++] = l_auto_stop_month  + '0';
    eventData[i++] = l_auto_stop_year + '0';
    eventData[i++] = l_auto_stop_hr + '0';
    eventData[i++] = l_auto_stop_min + '0';
    return i;
}

int encodeEventIdWaterDetect(char *eventData)
{
    int i =0;
    eventData[i++] = eventId_Water_Detect_Time + '0';
    
    eventData[i++] = l_water_detect_date + '0';
    eventData[i++] = l_water_detect_month  + '0';
    eventData[i++] = l_water_detect_year + '0';
    eventData[i++] = l_water_detect_hr + '0';
    eventData[i++] = l_water_detect_min + '0';    return i;
}

int encodeEventIdWaterLost(char *eventData)
{
    int i =0;
    eventData[i++] = eventId_Water_Lost_Time + '0';
    
    eventData[i++] = l_water_lost_date + '0';
    eventData[i++] = l_water_lost_month  + '0';
    eventData[i++] = l_water_lost_year + '0';
    eventData[i++] = l_water_lost_hr + '0';
    eventData[i++] = l_water_lost_min + '0';

    return i;
}

int encodeEventIdFullDetect(char *eventData)
{
    int i =0;
    eventData[i++] = eventId_Full_Detect_Time + '0';
    

    eventData[i++] = l_tank_full_detect_date + '0';
    eventData[i++] = l_tank_full_detect_month  + '0';
    eventData[i++] = l_tank_full_detect_year + '0';
    eventData[i++] = l_tank_full_detect_hr + '0';
    eventData[i++] = l_tank_full_detect_min + '0';
    
    return i;
}

int encodeEventIdFullLost(char *eventData)
{
    int i =0;
    eventData[i++] = eventId_Full_Lost_Time + '0';
    

    eventData[i++] = l_tank_full_lost_date + '0';
    eventData[i++] = l_tank_full_lost_month  + '0';
    eventData[i++] = l_tank_full_lost_year + '0';
    eventData[i++] = l_tank_full_lost_hr + '0';
    eventData[i++] = l_tank_full_lost_min + '0';

    return i;
}


int encodeEventIdHighDetect(char *eventData)
{
    int i =0;
    eventData[i++] = eventId_High_Detect_Time + '0';
    
    eventData[i++] = l_tank_high_detect_date + '0';
    eventData[i++] = l_tank_high_detect_month  + '0';
    eventData[i++] = l_tank_high_detect_year + '0';
    eventData[i++] = l_tank_high_detect_hr + '0';
    eventData[i++] = l_tank_high_detect_min + '0';
    
    return i;
}

int encodeEventIdHighLost(char *eventData)
{
    int i =0;
    eventData[i++] = eventId_High_Lost_Time + '0';
    
    eventData[i++] = l_tank_high_lost_date + '0';
    eventData[i++] = l_tank_high_lost_month  + '0';
    eventData[i++] = l_tank_high_lost_year + '0';
    eventData[i++] = l_tank_high_lost_hr + '0';
    eventData[i++] = l_tank_high_lost_min + '0';

    return i;
}

int encodeEventIdMidDetect(char *eventData)
{
    int i =0;
    eventData[i++] = eventId_Mid_Detect_Time + '0';
    
    eventData[i++] = l_tank_mid_detect_date + '0';
    eventData[i++] = l_tank_mid_detect_month  + '0';
    eventData[i++] = l_tank_mid_detect_year + '0';
    eventData[i++] = l_tank_mid_detect_hr + '0';
    eventData[i++] = l_tank_mid_detect_min + '0';
    
    return i;
}

int encodeEventIdMidLost(char *eventData)
{
    int i =0;
    eventData[i++] = eventId_Mid_Lost_Time + '0';
    
    eventData[i++] = l_tank_mid_lost_date + '0';
    eventData[i++] = l_tank_mid_lost_month  + '0';
    eventData[i++] = l_tank_mid_lost_year + '0';
    eventData[i++] = l_tank_mid_lost_hr + '0';
    eventData[i++] = l_tank_mid_lost_min + '0';

    return i;
}


int encodeEventIdLowDetect(char *eventData)
{
    int i =0;
    eventData[i++] = eventId_Low_Detect_Time + '0';
    
    eventData[i++] = l_tank_low_detect_date + '0';
    eventData[i++] = l_tank_low_detect_month  + '0';
    eventData[i++] = l_tank_low_detect_year + '0';
    eventData[i++] = l_tank_low_detect_hr + '0';
    eventData[i++] = l_tank_low_detect_min + '0';
    
    return i;
}

int encodeEventIdLowLost(char *eventData)
{
    int i =0;
    eventData[i++] = eventId_Low_Lost_Time + '0';
    
    eventData[i++] = l_tank_low_lost_date + '0';
    eventData[i++] = l_tank_low_lost_month  + '0';
    eventData[i++] = l_tank_low_lost_year + '0';
    eventData[i++] = l_tank_low_lost_hr + '0';
    eventData[i++] = l_tank_low_lost_min + '0';

    return i;
}

int encodeEventIdManual(char *eventData)
{
    int i =0;
    eventData[i++] = eventId_Mode_Manual_Time + '0';
    
    eventData[i++] = l_mode_manual_date + '0';
    eventData[i++] = l_mode_manual_month  + '0';
    eventData[i++] = l_mode_manual_year + '0';
    eventData[i++] = l_mode_manual_hr + '0';
    eventData[i++] = l_mode_manual_min + '0';

    return i;
}

int encodeEventIdAuto(char *eventData)
{
    int i =0;
    eventData[i++] = eventId_Mode_Auto_Time + '0';
    
    eventData[i++] = l_mode_auto_date + '0';
    eventData[i++] = l_mode_auto_month  + '0';
    eventData[i++] = l_mode_auto_year + '0';
    eventData[i++] = l_mode_auto_hr + '0';
    eventData[i++] = l_mode_auto_min + '0';

    return i;
}


void getAllStatsFromEEPROM()
{
    get_motor_start_time();
    get_motor_stop_time();
  
    get_auto_start_time();
    get_auto_stop_time();

    get_water_detect_time();
    get_water_lost_time();

    get_full_level_detect_time();
    get_full_level_lost_time();
  
    get_high_level_detect_time();
    get_high_level_lost_time();
    
    get_mid_level_detect_time();
    get_mid_level_lost_time();

    get_low_level_detect_time();
    get_low_level_lost_time();

    get_mode_manual_time();
    get_mode_auto_time();  
}

void initAllStatsFromEEPROM()
{
    init_motor_start_time(MOTOR_START_REASON_APP_TRIGGER);
    init_motor_stop_time(MOTOR_STOP_REASON_APP_TRIGGER );
  
    init_auto_start_time();
    init_auto_stop_time();

    init_water_detect_time();
    init_water_lost_time();

    init_full_level_detect_time();
    init_full_level_lost_time();
  
    init_high_level_detect_time();
    init_high_level_lost_time();
    
    init_mid_level_detect_time();
    init_mid_level_lost_time();

    init_low_level_detect_time();
    init_low_level_lost_time();

    init_mode_manual_time();
    init_mode_auto_time();

}



void save_low_level_detect_time()
{
  save_sensor_time(EEPROM_POS_LOW_DETECT_TIME, &l_tank_low_detect_date, &l_tank_low_detect_month, &l_tank_low_detect_year, &l_tank_low_detect_hr, &l_tank_low_detect_min);
}
void init_low_level_detect_time()
{
  init_sensor_time(EEPROM_POS_LOW_DETECT_TIME, &l_tank_low_detect_date, &l_tank_low_detect_month, &l_tank_low_detect_year, &l_tank_low_detect_hr, &l_tank_low_detect_min);
}
void get_low_level_detect_time()
{
  read_sensor_time(EEPROM_POS_LOW_DETECT_TIME, &l_tank_low_detect_date, &l_tank_low_detect_month, &l_tank_low_detect_year, &l_tank_low_detect_hr, &l_tank_low_detect_min);
}


void save_low_level_lost_time()
{
  save_sensor_time(EEPROM_POS_LOW_LOST_TIME, &l_tank_low_lost_date, &l_tank_low_lost_month, &l_tank_low_lost_year, &l_tank_low_lost_hr, &l_tank_low_lost_min);
}
void init_low_level_lost_time()
{
  init_sensor_time(EEPROM_POS_LOW_LOST_TIME, &l_tank_low_lost_date, &l_tank_low_lost_month, &l_tank_low_lost_year, &l_tank_low_lost_hr, &l_tank_low_lost_min);
}
void get_low_level_lost_time()
{
  read_sensor_time(EEPROM_POS_LOW_LOST_TIME, &l_tank_low_lost_date, &l_tank_low_lost_month, &l_tank_low_lost_year, &l_tank_low_lost_hr, &l_tank_low_lost_min);
}



void save_mid_level_detect_time()
{
  save_sensor_time(EEPROM_POS_MID_DETECT_TIME, &l_tank_mid_detect_date, &l_tank_mid_detect_month, &l_tank_mid_detect_year, &l_tank_mid_detect_hr, &l_tank_mid_detect_min);
}
void init_mid_level_detect_time()
{
  init_sensor_time(EEPROM_POS_MID_DETECT_TIME, &l_tank_mid_detect_date, &l_tank_mid_detect_month, &l_tank_mid_detect_year, &l_tank_mid_detect_hr, &l_tank_mid_detect_min);
}
void get_mid_level_detect_time()
{
  read_sensor_time(EEPROM_POS_MID_DETECT_TIME, &l_tank_mid_detect_date, &l_tank_mid_detect_month, &l_tank_mid_detect_year, &l_tank_mid_detect_hr, &l_tank_mid_detect_min);
}


void save_mid_level_lost_time()
{
  save_sensor_time(EEPROM_POS_MID_LOST_TIME, &l_tank_mid_lost_date, &l_tank_mid_lost_month, &l_tank_mid_lost_year, &l_tank_mid_lost_hr, &l_tank_mid_lost_min);
}
void init_mid_level_lost_time()
{
  init_sensor_time(EEPROM_POS_MID_LOST_TIME, &l_tank_mid_lost_date, &l_tank_mid_lost_month, &l_tank_mid_lost_year, &l_tank_mid_lost_hr, &l_tank_mid_lost_min);
}
void get_mid_level_lost_time()
{
  read_sensor_time(EEPROM_POS_MID_LOST_TIME, &l_tank_mid_lost_date, &l_tank_mid_lost_month, &l_tank_mid_lost_year, &l_tank_mid_lost_hr, &l_tank_mid_lost_min);
}


void save_high_level_detect_time()
{
  save_sensor_time(EEPROM_POS_HIGH_DETECT_TIME, &l_tank_high_detect_date, &l_tank_high_detect_month, &l_tank_high_detect_year, &l_tank_high_detect_hr, &l_tank_high_detect_min);
}
void init_high_level_detect_time()
{
  init_sensor_time(EEPROM_POS_HIGH_DETECT_TIME, &l_tank_high_detect_date, &l_tank_high_detect_month, &l_tank_high_detect_year, &l_tank_high_detect_hr, &l_tank_high_detect_min);
}
void get_high_level_detect_time()
{
  read_sensor_time(EEPROM_POS_HIGH_DETECT_TIME, &l_tank_high_detect_date, &l_tank_high_detect_month, &l_tank_high_detect_year, &l_tank_high_detect_hr, &l_tank_high_detect_min);
}


void save_high_level_lost_time()
{
  save_sensor_time(EEPROM_POS_HIGH_LOST_TIME, &l_tank_high_lost_date, &l_tank_high_lost_month, &l_tank_high_lost_year, &l_tank_high_lost_hr, &l_tank_high_lost_min);
}
void init_high_level_lost_time()
{
  init_sensor_time(EEPROM_POS_HIGH_LOST_TIME, &l_tank_high_lost_date, &l_tank_high_lost_month, &l_tank_high_lost_year, &l_tank_high_lost_hr, &l_tank_high_lost_min);
}
void get_high_level_lost_time()
{
  read_sensor_time(EEPROM_POS_HIGH_LOST_TIME, &l_tank_high_lost_date, &l_tank_high_lost_month, &l_tank_high_lost_year, &l_tank_high_lost_hr, &l_tank_high_lost_min);
}


void save_full_level_detect_time()
{
  save_sensor_time(EEPROM_POS_FULL_DETECT_TIME, &l_tank_full_detect_date, &l_tank_full_detect_month, &l_tank_full_detect_year, &l_tank_full_detect_hr, &l_tank_full_detect_min);
}
void init_full_level_detect_time()
{
  init_sensor_time(EEPROM_POS_FULL_DETECT_TIME, &l_tank_full_detect_date, &l_tank_full_detect_month, &l_tank_full_detect_year, &l_tank_full_detect_hr, &l_tank_full_detect_min);
}
void get_full_level_detect_time()
{
  read_sensor_time(EEPROM_POS_FULL_DETECT_TIME, &l_tank_full_detect_date, &l_tank_full_detect_month, &l_tank_full_detect_year, &l_tank_full_detect_hr, &l_tank_full_detect_min);
}


void save_full_level_lost_time()
{
  save_sensor_time(EEPROM_POS_FULL_LOST_TIME, &l_tank_full_lost_date, &l_tank_full_lost_month, &l_tank_full_lost_year, &l_tank_full_lost_hr, &l_tank_full_lost_min);
}
void init_full_level_lost_time()
{
  init_sensor_time(EEPROM_POS_FULL_LOST_TIME, &l_tank_full_lost_date, &l_tank_full_lost_month, &l_tank_full_lost_year, &l_tank_full_lost_hr, &l_tank_full_lost_min);
}
void get_full_level_lost_time()
{
  read_sensor_time(EEPROM_POS_FULL_LOST_TIME, &l_tank_full_lost_date, &l_tank_full_lost_month, &l_tank_full_lost_year, &l_tank_full_lost_hr, &l_tank_full_lost_min);
}


void save_water_detect_time()
{
  save_sensor_time(EEPROM_POS_WATER_DETECT_TIME, &l_water_detect_date, &l_water_detect_month, &l_water_detect_year, &l_water_detect_hr, &l_water_detect_min);
}
void init_water_detect_time()
{
  init_sensor_time(EEPROM_POS_WATER_DETECT_TIME, &l_water_detect_date, &l_water_detect_month, &l_water_detect_year, &l_water_detect_hr, &l_water_detect_min);
}
void get_water_detect_time()
{
  read_sensor_time(EEPROM_POS_WATER_DETECT_TIME, &l_water_detect_date, &l_water_detect_month, &l_water_detect_year, &l_water_detect_hr, &l_water_detect_min);
}

void save_water_lost_time()
{
  save_sensor_time(EEPROM_POS_WATER_LOST_TIME, &l_water_lost_date, &l_water_lost_month, &l_water_lost_year, &l_water_lost_hr, &l_water_lost_min);
}
void init_water_lost_time()
{
  init_sensor_time(EEPROM_POS_WATER_LOST_TIME, &l_water_lost_date, &l_water_lost_month, &l_water_lost_year, &l_water_lost_hr, &l_water_lost_min);
}
void get_water_lost_time()
{
  read_sensor_time(EEPROM_POS_WATER_LOST_TIME, &l_water_lost_date, &l_water_lost_month, &l_water_lost_year, &l_water_lost_hr, &l_water_lost_min);
}

void save_auto_start_time()
{
  save_sensor_time(EEPROM_POS_AUTO_START_TIME, &l_auto_start_date, &l_auto_start_month, &l_auto_start_year, &l_auto_start_hr, &l_auto_start_min);
}
void init_auto_start_time()
{
  init_sensor_time(EEPROM_POS_AUTO_START_TIME, &l_auto_start_date, &l_auto_start_month, &l_auto_start_year, &l_auto_start_hr, &l_auto_start_min);
}
void get_auto_start_time()
{
  read_sensor_time(EEPROM_POS_AUTO_START_TIME, &l_auto_start_date, &l_auto_start_month, &l_auto_start_year, &l_auto_start_hr, &l_auto_start_min);
}

void save_auto_stop_time()
{
  save_sensor_time(EEPROM_POS_AUTO_STOP_TIME, &l_auto_stop_date, &l_auto_stop_month, &l_auto_stop_year, &l_auto_stop_hr, &l_auto_stop_min);
}
void init_auto_stop_time()
{
  save_sensor_time(EEPROM_POS_AUTO_STOP_TIME, &l_auto_stop_date, &l_auto_stop_month, &l_auto_stop_year, &l_auto_stop_hr, &l_auto_stop_min);
}
void get_auto_stop_time()
{
  read_sensor_time(EEPROM_POS_AUTO_STOP_TIME, &l_auto_stop_date, &l_auto_stop_month, &l_auto_stop_year, &l_auto_stop_hr, &l_auto_stop_min);
}





void save_motor_start_time(byte reason)
{
  //Serial.println ("save_motor_start_time\r\n");
  save_sensor_time(EEPROM_POS_MOTOR_START_TIME, &l_motor_start_date, &l_motor_start_month, &l_motor_start_year, &l_motor_start_hr, &l_motor_start_min);
  l_motor_start_reason = reason;

  //EEPROM.update(EEPROM_POS_MOTOR_START_TIME+5, l_motor_start_reason);
}
void init_motor_start_time(byte reason)
{
  init_sensor_time(EEPROM_POS_MOTOR_START_TIME, &l_motor_start_date, &l_motor_start_month, &l_motor_start_year, &l_motor_start_hr, &l_motor_start_min);
  l_motor_start_reason = reason;
  //EEPROM.update(EEPROM_POS_MOTOR_START_TIME+5, l_motor_start_reason);
}
void get_motor_start_time()
{
  read_sensor_time(EEPROM_POS_MOTOR_START_TIME, &l_motor_start_date, &l_motor_start_month, &l_motor_start_year, &l_motor_start_hr, &l_motor_start_min);
  //l_motor_start_reason = EEPROM.read(EEPROM_POS_MOTOR_START_TIME+5);
}

void save_motor_stop_time(byte reason)
{
  save_sensor_time(EEPROM_POS_MOTOR_STOP_TIME, &l_motor_stop_date, &l_motor_stop_month, &l_motor_stop_year, &l_motor_stop_hr, &l_motor_stop_min);
  l_motor_stop_reason = reason;
  //EEPROM.update(EEPROM_POS_MOTOR_STOP_TIME+5, l_motor_stop_reason);
}
void init_motor_stop_time(byte reason)
{
  init_sensor_time(EEPROM_POS_MOTOR_STOP_TIME, &l_motor_stop_date, &l_motor_stop_month, &l_motor_stop_year, &l_motor_stop_hr, &l_motor_stop_min);
  l_motor_stop_reason = reason;
  //EEPROM.update(EEPROM_POS_MOTOR_STOP_TIME+5, l_motor_stop_reason);
}
void get_motor_stop_time()
{
  read_sensor_time(EEPROM_POS_MOTOR_STOP_TIME, &l_motor_stop_date, &l_motor_stop_month, &l_motor_stop_year, &l_motor_stop_hr, &l_motor_stop_min);
  //l_motor_stop_reason = EEPROM.read(EEPROM_POS_MOTOR_STOP_TIME+5);
}



void save_mode_manual_time()
{
  save_sensor_time(EEPROM_POS_MODE_MANUAL_TIME, &l_mode_manual_date, &l_mode_manual_month, &l_mode_manual_year, &l_mode_manual_hr, &l_mode_manual_min);
}
void init_mode_manual_time()
{
  save_sensor_time(EEPROM_POS_MODE_MANUAL_TIME, &l_mode_manual_date, &l_mode_manual_month, &l_mode_manual_year, &l_mode_manual_hr, &l_mode_manual_min);
}
void get_mode_manual_time()
{
  read_sensor_time(EEPROM_POS_MODE_MANUAL_TIME, &l_mode_manual_date, &l_mode_manual_month, &l_mode_manual_year, &l_mode_manual_hr, &l_mode_manual_min);
}


void save_mode_auto_time()
{
  save_sensor_time(EEPROM_POS_MODE_AUTO_TIME, &l_mode_auto_date, &l_mode_auto_month, &l_mode_auto_year, &l_mode_auto_hr, &l_mode_auto_min);
}
void init_mode_auto_time()
{
  save_sensor_time(EEPROM_POS_MODE_AUTO_TIME, &l_mode_auto_date, &l_mode_auto_month, &l_mode_auto_year, &l_mode_auto_hr, &l_mode_auto_min);
}
void get_mode_auto_time()
{
  read_sensor_time(EEPROM_POS_MODE_AUTO_TIME, &l_mode_auto_date, &l_mode_auto_month, &l_mode_auto_year, &l_mode_auto_hr, &l_mode_auto_min);
}



//generic function
void save_sensor_time(int pos, byte *ldate, byte *lmonth, byte *lyear, byte *lhr, byte *lminute)
{
   *ldate = date;
   *lmonth = month;
   *lyear = year;
   *lhr = hour;
   *lminute = minute;  
    init_sensor_time (pos,ldate, lmonth, lyear, lhr, lminute);
}


//generic function
void init_sensor_time(int pos, byte *ldate, byte *lmonth, byte *lyear, byte *lhr, byte *lminute)
{
    EEPROM.update(pos, *ldate);
    pos++;
    EEPROM.update(pos, *lmonth);
    pos++;
    EEPROM.update(pos, *lyear);
    pos++;
    EEPROM.update(pos, *lhr);
    pos++;
    EEPROM.update(pos, *lminute);
    pos++;
}

void read_sensor_time(int pos, byte *ldate, byte *lmonth, byte *lyear, byte *lhr, byte *lminute)
{
    *ldate = EEPROM.read(pos);
    pos++;
    *lmonth = EEPROM.read(pos);
    pos++;
    *lyear = EEPROM.read(pos);
    pos++;
    *lhr = EEPROM.read(pos);
    pos++;
    *lminute = EEPROM.read(pos);
}


// Convert normal decimal numbers to binary coded decimal 
byte decToBcd(byte val) 
{ 
   return( (val/10*16) + (val%10) ); 
} 
// Convert binary coded decimal to normal decimal numbers 
byte bcdToDec(byte val) 
{ 
   return( (val/16*10) + (val%16) ); 
} 
 
//RCSwitch mySwitch = RCSwitch(); 
void setDS3231time(byte second, byte minute, byte hour, byte dayOfWeek, byte dayOfMonth, byte month, byte year) 
{ 
  //Serial.print ("\r\nsetting Time");
   // sets time and date data to DS3231 
   Wire.beginTransmission(DS3231_I2C_ADDRESS); 
   Wire.write(0); // set next input to start at the seconds register 
   Wire.write(decToBcd(second)); // set seconds 
   Wire.write(decToBcd(minute)); // set minutes 
   Wire.write(decToBcd(hour)); // set hours 
   Wire.write(decToBcd(dayOfWeek)); // set day of week (1=Sunday, 7=Saturday) 
   Wire.write(decToBcd(dayOfMonth)); // set date (1 to 31) 
   Wire.write(decToBcd(month)); // set month 
   Wire.write(decToBcd(year)); // set year (0 to 99) 
   Wire.endTransmission();
   //Serial.print ("\r\nSet time Done"); 

/*
    Serial.print("Setting Current Time as ");
    Serial.print(dayOfMonth);
    Serial.print("-");
    Serial.print(month);
    Serial.print("-");
    Serial.print(year);
    Serial.print(" ");
    Serial.print(hour);
    Serial.print(":");
    Serial.print(minute);
    Serial.print(":");
    Serial.print(second);
    Serial.print("\r\n");
*/
} 

void readDS3231time(
  byte *second, 
  byte *minute, 
  byte *hour, 
  byte *dayOfWeek, 
  byte *dayOfMonth, 
  byte *month, 
  byte *year) 
{ 
   Wire.beginTransmission(DS3231_I2C_ADDRESS); 
   Wire.write(0); // set DS3231 register pointer to 00h 
   Wire.endTransmission(); 
   Wire.requestFrom(DS3231_I2C_ADDRESS, 7); 
   // request seven bytes of data from DS3231 starting from register 00h 
   *second = bcdToDec(Wire.read() & 0x7f); 
   *minute = bcdToDec(Wire.read()); 
   *hour = bcdToDec(Wire.read() & 0x3f); 
   *dayOfWeek = bcdToDec(Wire.read()); 
   *dayOfMonth = bcdToDec(Wire.read()); 
   *month = bcdToDec(Wire.read()); 
   *year = bcdToDec(Wire.read());

/*
    Serial.print("Time is ");
    Serial.print(*dayOfMonth);
    Serial.print("-");
    Serial.print(*month);
    Serial.print("-");
    Serial.print(*year);
    Serial.print(" ");
    Serial.print(*hour);
    Serial.print(":");
    Serial.print(*minute);
    Serial.print(":");
    Serial.print(*second);
    Serial.print("\r\n");
  */   
} 

//byte second, minute, hour, dayOfWeek, dayOfMonth, month; 
//byte dayOfWeek, dayOfMonth; 

boolean isTimeToTurnMotorOnLed() 
{ 
   byte next_hour, next_min;
   // retrieve data from DS3231 
   readDS3231time(&second, &minute, &hour, &day, &date, &month, &year);
   //Serial.print("Current Time is-->");
   //Serial.print(hour);   
   //Serial.print(":");
   //Serial.print(minute);
   //Check if time to turn on motor
   int iteration = 0;
   boolean result = false;
   boolean isOddHour = false;
   /*
   for (iteration = 0;iteration < MAXODDHOURS; iteration++)
   {      
       if (ODDHOURS[iteration]==hour) //commented temporarily
       {
          //Serial.print ("Odd Hour is True");
          isOddHour = true;
          break;
       }
   }
   */

   result = is_Time_to_Run_Motor(hour, minute, second);

   if (isTankFilled==false)
   {
      /*if( isTankLevelHigh==true) // no need to run timed motor - if water level is high
      {
          result = false;
      }*/
      
      /*
      else // if tank is not high and it is not an odd hour - only then check the time to run:
      {  
           result = is_Time_to_Run_Motor(hour, minute, second);
      }*/
   }
   else //tank full
   {
      result = false;
   } 
   if (isWaterInFlow == true)
   {
      if (isTankFilled ==true)
      {
          result = false; 
      }        
      // even if water is there and tank is not full - but greater than high - it is possible
      //a) Motor just stopped due to tank getting full - and tank is little less than full - here it shall not be a case to run motor. - give some guard time- after tankfull - bare minimum time - before restarting the motor
      //b) Motor just stopped due to water gone - 2 minutes of inactivity - now suddenly water is again there - it makes sense to start the motor here.
      else if (isTankLevelHigh == true) //No need to run motor in case of high level -even if water is there.
      {
          result = false;
      }
      else //water is less than high and water is detected - immediately start the motor
      {
             result = true;
             //Serial.print("Motor should start, waterflow is detected /n");
            /*
            if (isOddHour == true) //In Odd - hours only if tank is near empty and water flow is there- it shall run. Otherwise not.
            {
                if (isTankLevelVeryLow == true)
                {
                  result = true;
                }
                else
                {
                  result = false;
                }
            }
            else
            {
                  result = true;
            }
            */
      }
   }

   /*
   //Following code shall be executed irrespective of all the situations to show time
    next_hour = 25;
    next_min = 61;
    current_time_in_minutes = hour * 60 + minute;
    minutes_remaining = 24*60;

    
    // commented temporarily
    for (iteration = 0;iteration <MAXITERATIONS; iteration++)
    {
          if (HOURS[iteration] < hour) //change of day in next hour
          {         
              new_minutes_remaining = HOURS[iteration]*60 + MINS[iteration] + ((24 - hour - 1) * 60 + 60 - minute);
          }
          else if ((HOURS[iteration] == hour) && (MINS[iteration] <= minute))
          {
             //This case is also a case of next day
             new_minutes_remaining = HOURS[iteration]*60 + MINS[iteration] + ((24 - hour - 1) * 60 + 60 - minute);
          }
          else
          {
             //next_new_time_in_minutes = HOURS[iteration]*60 + MINS[iteration];
             new_minutes_remaining = HOURS[iteration]*60 + MINS[iteration] - hour *60 - minute; 
          }          
          
          if (new_minutes_remaining < minutes_remaining)
          {
            minutes_remaining = new_minutes_remaining;
            next_hour = HOURS[iteration];
            next_min = MINS[iteration];
          }
    }
    */
    //only temporary.  Need to comment next two lines when above function is uncommented.
    //next_hour = hour;
    //next_min = minute;   
   if ( (result == true) && (motorSwitchCondition==true))
   {
      result = false; //motor is already running so return false
   }

   //Serial.print("Decision to run motor -->");
   //Serial.print(result);
   return result;
 } 

int motorOffReason;
int motorOnReason;
void turnMotorOff(int reason)
{
    if (motorSwitchCondition==true)
    {
        //Serial.print ("Motor Turning Off \r\n");
        digitalWrite(bulb, HIGH); 
        significantTimeMotorRunningWithoutWater = 0;  
        isSpeakerOn = true;
        motorSwitchCondition = false;
        
        save_motor_stop_time(reason);
        motorOffReason = reason;
        if((reason == MOTOR_STOP_REASON_MANUAL_TRIGGER) ||
            (reason == MOTOR_STOP_REASON_APP_TRIGGER ) )
        {
            sendEventToDevice(eventId_Motor_Stop_Time);
        }
        else
        {
            sendEventToDevice(eventId_Auto_Stop_Time);
            save_auto_stop_time();
        }
        
        //sendStatsToDevice();
        //sendtoDevice();
    
        speakerOnTimeMillis = millis();
        motorStopTune();
    } 
    else
    {
         //Serial.print ("Motor Already Switched off\r\n");
    }
    motorSwitchCondition = false;
     
    //motorOnMins = 0;
    //motorStartMillis = millis();
}

void turnMotorOn(int reason)
{

    if (motorSwitchCondition == false)
    {
       //Serial.print ("Motor Turning On \r\n");
       //digitalWrite(motorSwitch, LOW); //Hack
       digitalWrite(bulb, LOW); 
       //Serial.println("#### Starting the motor ### ");

       significantTimeMotorRunningWithoutWater = 0;
       waterGoneTimeMillis = millis();
       motorOnReason = reason;
       motorSwitchCondition = true;
       save_motor_start_time(reason);

        if((reason == MOTOR_START_REASON_MANUAL_TRIGGER) ||
            (reason == MOTOR_START_REASON_APP_TRIGGER ) )
        {
            sendEventToDevice(eventId_Motor_Start_Time);
        }
        else
        {
            sendEventToDevice(eventId_Auto_Start_Time);
            save_auto_start_time();
        }
        //sendtoDevice();    
    
       speakerOnTimeMillis = waterGoneTimeMillis;
       motorStopTune();
    }
    motorSwitchCondition = true;
}

void motorStartTune()
{
   analogWrite(speakerOut, 0);     
   for (count = 0; count < 20; count++) 
   {
      statePin = !statePin;
      //digitalWrite(ledPin, statePin);
      for (count3 = 0; count3 <= (melody[count*2] - 48) * 30*8; count3++) 
      {
        for (count2 = 0; count2 < 8; count2++) 
        {
           if (names[count2] == melody[count*2 + 1]) 
           {       
              analogWrite(speakerOut,500);
              delayMicroseconds(tones[count2]);
              analogWrite(speakerOut, 0);
              delayMicroseconds(tones[count2]);
           } 
           if (melody[count*2 + 1] == 'p') 
           {
              // make a pause of a certain size
              analogWrite(speakerOut, 0);
              delayMicroseconds(500);
           }
        }
     }
   }
  
}

/*
void motorStartTune()
{
 // Set up a counter to pull from melody[] and beats[]
  for (int i=0; i<MAX_COUNT_OFF; i++) 
  {
    tone_ = melody_off[i];
    beat = beats_off[i];

    duration = beat * tempo; // Set up timing

    playTone(); 
    // A pause between notes...
    delayMicroseconds(300);
  }
}
*/

void motorStopTune()
{
  
 // Set up a counter to pull from melody[] and beats[]
  for (int i=0; i<MAX_COUNT_OFF; i++) 
  {
    tone_ = melody_off[i];
    beat = beats_off[i];

    duration = beat * tempo; // Set up timing

    playTone(); 
    // A pause between notes...
    delayMicroseconds(pause);
  }
  
}


void LevelChangeTune()
{
  
 // Set up a counter to pull from melody[] and beats[]
  for (int i=0; i<MAX_COUNT_LEVELCHANGE; i++) 
  {
    tone_ = melody_level[i];
    beat = beats_level[i];

    duration = beat * tempo; // Set up timing

    playTone(); 
    // A pause between notes...
    delayMicroseconds(pause);
  }
}


int pauseLowLEvel = 100;

void veryLowLevelTune()
{
  
 // Set up a counter to pull from melody[] and beats[]
  for (int i=0; i<MAX_COUNT_LEVELCHANGE; i++) 
  {
    tone_ = melody_level[i];
    beat = beats_level[i];

    duration = beat * tempo; // Set up timing

    playTone(); 
    // A pause between notes...
    delayMicroseconds(pauseLowLEvel);
  }
}

void motorStopTune2()
{
  
  analogWrite(speakerOut, 0);     
   for (count = 0; count < MAX_COUNT; count++) 
   {
      statePin = !statePin;
      //digitalWrite(ledPin, statePin);
      for (count3 = 0; count3 <= (melody[count*2] - 48) * 30; count3++) 
      {
        for (count2 = 0; count2 < 8; count2++) 
        {
           if (names[count2] == melody[count*2 + 1]) 
           {       
              analogWrite(speakerOut,500);
              delayMicroseconds(tones[count2]);
              analogWrite(speakerOut, 0);
              delayMicroseconds(tones[count2]);
           } 
           if (melody[count*2 + 1] == 'p') 
           {
              // make a pause of a certain size
              analogWrite(speakerOut, 0);
              delayMicroseconds(500);
           }
        }
     }
   }
}
void waterFlowDetectedTune()
{

}

void waterFlowGoneTune()
{
 
}


void decodeandSetTime(char * data, int maxbyte)
{
  byte secs = 0;
  byte mins = 0;
  byte hrs = 0;
  byte days = 0;
  byte dates = 0;
  byte months = 0;
  byte years = 0;
  
  
   int i = 0;
   i++; //for command '1'

   if (maxbyte < i ) return;
            secs = (byte)(data[i++] - (char)('0'));
   if (maxbyte < i ) return;
            mins = (byte)(data[i++] - (char)('0'));
   if (maxbyte < i ) return;
            hrs = (byte)(data[i++] - (char)('0'));
   if (maxbyte < i ) return;
            days = (byte)(data[i++] - (char)('0'));
   if (maxbyte < i ) return;
            dates = (byte)(data[i++] - (char)('0'));
   if (maxbyte < i ) return;
            months = (byte)(data[i++] - (char)('0'));
   if (maxbyte < i ) return;
            years = (byte)(data[i++] - (char)('0'));
   
   setDS3231time(secs, mins, hrs, days, dates, months, years);
   //Serial.println("#### Time has been set correctly at device ### ");
   return;
}

void decodeandSetProfile(char * data, int maxbyte)
{  
   int i = 0;
   i++; //for command '1'
   if (maxbyte < i ) return;
            mor_start_hr = (byte)(data[i++] - (char)('0'));
   if (maxbyte < i ) return;
            mor_start_min = (byte)(data[i++] - (char)('0'));
   if (maxbyte < i ) return;
            mor_max_retry = (byte)(data[i++] - (char)('0'));
   if (maxbyte < i ) return;
            eve_start_hr = (byte)(data[i++] - (char)('0'));
   if (maxbyte < i ) return;
            eve_start_min = (byte)(data[i++] - (char)('0'));
   if (maxbyte < i ) return;
            eve_max_retry = (byte)(data[i++] - (char)('0'));
   if (maxbyte < i ) return;
            retry_interval = (byte)(data[i++] - (char)('0'));
   if (maxbyte < i ) return;
            MAXMINSWITHOUTWATER = (byte)(data[i++] - (char)('0'));
            
   
   change_motor_profile();
   get_motor_profile();
   //Serial.println("#### Time has been set correctly at device ### ");
   return;
}

void encodeStatus()
{
    int i =0;
    serialSendData[i++] = 'a';
    serialSendData[i++] = 'b';
    serialSendData[i++] = 'c';
    serialSendData[i++] = isTankFilled + '0';
    serialSendData[i++] = isTankLevelHigh  + '0';
    serialSendData[i++] = isTankMid + '0';
    serialSendData[i++] = isTankLow + '0';
    serialSendData[i++] = isTankLevelVeryLow + '0';
    serialSendData[i++] = isWaterInFlow + '0';
    serialSendData[i++] = motorSwitchCondition + '0';
    serialSendData[i++] = motorOnWithoutWaterMins + '0';
    serialSendData[i++] = manualModeState + '0';
    serialSendData[i++] = second + '0'; 
    serialSendData[i++] = minute + '0';//seems ok
    serialSendData[i++] = hour + '0';//wrong

    //Serial.println(hour, DEC);
    //Serial.println(":");
    //Serial.println(minute, DEC);
    //Serial.println(":");
    //Serial.println(second, DEC);
    //Serial.println(year, DEC);
    serialSendData[i++] = day + '0';
    serialSendData[i++] = date + '0';
    serialSendData[i++] = month + '0';
    serialSendData[i++] = year + '0';
    serialSendData[i++] = mor_start_hr + '0';
    serialSendData[i++] = mor_start_min + '0';
    serialSendData[i++] = mor_max_retry + '0';
    serialSendData[i++] = eve_start_hr + '0';
    serialSendData[i++] = eve_start_min + '0';
    serialSendData[i++] = eve_max_retry + '0';
    serialSendData[i++] = retry_interval + '0';
    serialSendData[i++] = MAXMINSWITHOUTWATER + '0';
}

char serialData[32];
byte com = 0, error = 0, timerCounter = 0;
boolean connected;

void bluettothAction()
{  
  if (Genotronex.available())
  {
    connected = true;
    // clear timeout
    com = timerCounter;

    //Genotronex.println("11\r\n");
    //Serial.println("11.........................");
    //Serial.println("22\r\n");

    int maxbyte = Genotronex.readBytesUntil('\n', serialData, 31);

    //Serial.println(serialData);
    //Serial.println("33\r\n");

    switch(serialData[0])
    {
        case '0':
          //digitalWrite(ledpin,1);
          //Serial.println("encoding.........................");
          sendtoDevicePeriodic();
          break;

        case '1':
           //Serial.println("Setting time at device");
           decodeandSetTime(serialData, maxbyte);
           break;
        
        case '2':
           //Serial.println("Motor on trigger");
           //consider as start of motor command
           //Serial.println("Turning Motor On");
           turnMotorOn(MOTOR_START_REASON_APP_TRIGGER );
           //sendEventToDevice(eventId_Motor_Start_Time); //This is also sent from inside the turnMotorOn
           break;

         case '3':
            //consider as stop of motor command
            //Serial.println("Turning Motor Off");
            // Serial.println("Motor off trigger");
            turnMotorOff(MOTOR_STOP_REASON_APP_TRIGGER );
            //sendEventToDevice(eventId_Motor_Stop_Time); //This is also sent from inside the turnMotorOff
            break;

          case '4':  //change profile
            //consider as stop of motor command
            //Serial.println("Changing Profile");
            decodeandSetProfile(serialData, maxbyte);
            break;

          case '5':  //response to stats - request://essentially the last events data:
            //consider as stop of motor command
            //Serial.println("LastEventRequest");
            sendLastEventsToDevice();
            break;
           
        default:
          break;
          // inform user of non existing command
          //Serial.println("Command not recognised");
    }
    memset(serialData, 0, sizeof(serialData));
  }
}

/**
 * This function makes ints out of the received serial data, the 2 first
 * characters are not counted as they consist of the command character and
 * a comma separating the first variable.
 *
 * @params command The whole serial data received as an address
 * @params returnValues The array where the return values go as an address
 * @params returnNumber The number of values to set inside the returnValues variable
 */
boolean parseCommand(char* command, int* returnValues, byte returnNumber)
{
  // parsing state machine
  byte i = 1, j = 0, sign = 0, ch = 0, number;
  int temp = 0;
  while(i++)
  {
    switch(*(command + i))
    {
    case '\0':
    case ',':
      // set return value
      if(ch != 0)
      {
        returnValues[j++] = sign?-temp:temp;
        sign = 0;
        temp = 0;
        ch = 0;
      }
      else
      {
        return false;
      }
      break;
    case '-':
      sign = 1;
      break;
    default:
      // convert string to int
      number = *(command + i) - '0';
      if(number < 0 || number > 9)
      {
        return false;
      }
      temp = temp * 10 + number;
      ch++;
    }

    // enough return values have been set
    if(j == returnNumber)
    {
      return true;
    }
    // end of command reached
    else if(*(command + i) == '\0')
    {
      return false;
    }
  }
}


ISR(TIMER2_OVF_vect)
{
  // 8e6Hz / 256 / 256 / 122 = 1Hz
  if(timerCounter++ < 122)
  {
    return;
  }

  // verify communication status
  if(com == 0)
  {
    // no data has been passed since last time
    // interpret as communication failure
    digitalWrite(ledSystemOn, LOW);
    //delay(10);
    digitalWrite(ledSystemOn, HIGH);
    
    reset();
  }
  com = 0;
  timerCounter = 0;
}

void reset()
{
  connected = false;
  //setSpeed(0, 0);
}




// The setup() method runs once, when the sketch starts
void setup() //
{ 
    //digitalWrite(bulb, HIGH);  
  pinMode(bulb, OUTPUT);
  //digitalWrite(bulb, LOW);  // turn on pullup resistors - Here HIGH means open, LOW means switch closed.
  digitalWrite(bulb, HIGH);
  
   
  //digitalWrite(hallsensor, HIGH);       // turn on pullup resistors - Here HIGH means open, LOW means switch closed.
  
  pinMode(tankFullSensor, INPUT_PULLUP); //Initialize digital pin 5 as an input
  //digitalWrite(tankFullSensor, HIGH);       // turn on pullup resistors - Here HIGH means open, LOW means siwtch closed.

  pinMode(highLevelSensor, INPUT_PULLUP); //Initialize digital pin 5 as an input
  //digitalWrite(highLevelSensor, HIGH);       // turn on pullup resistors - Here HIGH means open, LOW means siwtch closed.
   
  pinMode(midLevelSensor, INPUT_PULLUP); //Initialize digital pin 5 as an input
  
  pinMode(lowLevelSensor, INPUT_PULLUP); //Initialize digital pin 5 as an input
  //digitalWrite(lowLevelSensor, HIGH);       // turn on pullup resistors - Here HIGH means open, LOW means siwtch closed.
 
  //digitalWrite(motorSwitch, HIGH); //https://arduino-info.wikispaces.com/ArduinoPower - mandates that relay works on low signal, also it says initialization sequence shall be first right and then set pin mode:
  //pinMode(motorSwitch, OUTPUT);

  //pinMode(fullLed, OUTPUT);

  pinMode(ledSystemOn, OUTPUT);
  
  pinMode(ledTankLow, OUTPUT);
  pinMode(ledTankMid, OUTPUT);
  pinMode(ledTankHigh, OUTPUT);
  pinMode(ledTankFull, OUTPUT);
  
  pinMode(ledWaterFlow, OUTPUT);
  pinMode(ledMotorOn, OUTPUT);

  digitalWrite(ledSystemOn, LOW);

  //digitalWrite(ledSystemOn, HIGH);
  digitalWrite(ledTankLow, LOW);
  digitalWrite(ledTankMid, LOW);
  digitalWrite(ledTankHigh, LOW);
  digitalWrite(ledTankFull, LOW);
  digitalWrite(ledWaterFlow, LOW);
  digitalWrite(ledMotorOn, LOW);


  pinMode(trigPin, OUTPUT); // Sets the trigPin as an Output
  pinMode(echoPin, INPUT); // Sets the echoPin as an Input

  pinMode(SirenOn, OUTPUT);
  digitalWrite(SirenOn, LOW);

  pinMode(forceMotorStartSignal, INPUT);
  digitalWrite(forceMotorStartSignal, HIGH); //// turn on pullup resistors - Here HIGH means open, LOW means siwtch closed.

  pinMode(motoMode, INPUT);
  digitalWrite(motoMode, LOW);
  
  //Time related functions:
  Wire.begin();
  
  Serial.begin(9600); //This is the setup function where the serial port is initialised,
 

  //attachInterrupt(0, rpm, RISING); //and the interrupt is attached for hallsensor
  //attachInterrupt(digitalPinToInterrupt(tankFullSensor), isTankFull, LOW);

  motorSwitchCondition = false;
  significantTimeMotorRunningWithoutWater = 0; 

    // set up the LCD's number of columns and rows:
   //lcd.begin(20, 4);
   // Print a message to the LCD.
   //lcd.print("Save Water & Power");
   
  //mySwitch.enableTransmit(10); 
  // set the initial time here: 
  // DS3231 seconds, minutes, hours, day, date, month, year 
  //setDS3231time(second, minute, hour, day, date, month, year);
  //setDS3231time(30,29,00,2,10,4,17);


   Genotronex.begin(9600);
   //Genotronex.println("Bluetooth On please press 1 or 0 blink LED ..");
   pinMode(ledpin,OUTPUT);

   if (is_eeprom_configured()==true)
   {
      //read from here
      get_motor_profile();
      get_version();
      //initAllStatsFromEEPROM();
      getAllStatsFromEEPROM();
   }
   else
   {
      //write here
      save_motor_profile();
      save_version();
      initAllStatsFromEEPROM();
   }

  pinMode(sensorPin, INPUT);
  digitalWrite(sensorPin, HIGH);
  pulseCount        = 0;
  flowRate          = 0.0;
  flowMilliLitres   = 0;
  totalMilliLitres  = 0;
  oldTime           = 0;

  // The Hall-effect sensor is connected to pin 2 which uses interrupt 0.
  // Configured to trigger on a FALLING state change (transition from HIGH
  // state to LOW state)
  attachInterrupt(sensorInterrupt, pulseCounter, FALLING);
  //pinMode(hallsensor, INPUT_PULLUP); //initializes digital pin 2 as an input

   
} 

// defines variables
long durationLevel;
int distance;
void processType2LevelSensors()
{
  /*
  // Clears the trigPin
  digitalWrite(trigPin, LOW);
  delayMicroseconds(2);
  // Sets the trigPin on HIGH state for 10 micro seconds
  digitalWrite(trigPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(trigPin, LOW);
  // Reads the echoPin, returns the sound wave travel time in microseconds
  durationLevel = pulseIn(echoPin, HIGH);
  // Calculating the distance
  distance= durationLevel*0.34/2;
  */
  //Serial.println((sonar.ping_median(10)/2) / 29.1);

  
  levelDistances[currentIndexlevelDistances] = (sonar.ping_median(10)/2) / 29.1 ;
  if (currentIndexlevelDistances == MaxLevelIndexes)
  {
    currentIndexlevelDistances=0; //reset to 0
    //Here calculate the mean value from the past stored readings:
    normalizeLevel();
    setLevels();
  }
  else
  {
    currentIndexlevelDistances++;
  }
  
}

float normalizedLevel = 0;

void normalizeLevel()
{
    int i=0;
    //If there is a reading outside 1cm range consider this as other baseValue. Assuming there can be max 3 different range of readings overall in any set of measured values
    float baseValue1 = levelDistances[0];
    byte weightValue1 = 0;
    float baseValue2 = levelDistances[0]; //dummy baseValue2
    byte weightValue2 = 0;
    float baseValue3 = levelDistances[0]; //dummy baseValue3
    byte weightValue3 = 0;
    
    for (i=0; i < MaxLevelIndexes; i++)
    {
          if ( (levelDistances[i] <= baseValue1) && (baseValue1 - levelDistances[i] > 1)  ||
               (levelDistances[i] >= baseValue1) && (levelDistances[i] - baseValue1 < 1 )  ) 
          
          {
              //within range ... increase the weight by 1
              weightValue1++; 
          }
          else
          {
            //check if basevalue2 is defined.
            if ( baseValue2 == baseValue1)//Init basevalue 2..if not already initialized
            {
               baseValue2 = levelDistances[i]; 
            }
            if ( (levelDistances[i] <= baseValue2) && (baseValue2 - levelDistances[i] > 1)  ||
               (levelDistances[i] >= baseValue2) && (levelDistances[i] - baseValue2 < 1 )  )
            {
                //within range ... increase the weight by 1
                weightValue2++; 
            }
            else
            {
                  //check if basevalue3 is defined.
                  if ( baseValue3 == baseValue1)//Init basevalue 3.. if not already initialized
                  {
                     baseValue3 = levelDistances[i]; 
                  }
                   if ( (levelDistances[i] <= baseValue3) && (baseValue3 - levelDistances[i] > 1)  ||
                      (levelDistances[i] >= baseValue3) && (levelDistances[i] - baseValue3 < 1 )  )
               
                  {
                      //within range ... increase the weight by 1
                      weightValue3++; 
                  }
            }
          }
    }
    //There can be two ways to know the levels:
    //1: whatever weight is maximum.. choose that value:::: means this seems to be the correct value
    //2: Or whatever baseValue is minimum.. chooise that value:: This should be the correct value.. other values can be due to wrong reflections from walls etc...
  
    normalizedLevel = 0;
    //if we choose method 1:
    if ( (weightValue1 >= weightValue2) && (weightValue1 >= weightValue3)) normalizedLevel = baseValue1;
    else if ( (weightValue2 >= weightValue1) && (weightValue2 >= weightValue3)) normalizedLevel = baseValue2;
    else if ( (weightValue3 >= weightValue1) && (weightValue3 >= weightValue2)) normalizedLevel = baseValue3;

/*
    Serial.print("BaseValues--> ");
    Serial.print( baseValue1);
    Serial.print(" ");
    Serial.print( baseValue2);
    Serial.print(" ");
    Serial.println( baseValue3);

    Serial.print("Weights--> ");
    Serial.print( weightValue1);
    Serial.print(" ");
    Serial.print( weightValue2);
    Serial.print(" ");
    Serial.println( weightValue3);
  */    
    //choose method 2 only if there is minimum 3 occurances of this type:
    if ( weightValue1>=3 && (baseValue1 <= baseValue2) && (baseValue1 <= baseValue3)) normalizedLevel = baseValue1;
    else if ( weightValue2>=3 && (baseValue2 <= baseValue3) && (baseValue2 <= baseValue1)) normalizedLevel = baseValue2;
    else if ( weightValue3=3 && (baseValue3 <= baseValue1) && (baseValue3 <= baseValue2)) normalizedLevel = baseValue3;

 /*
    Serial.print( "Final Level -->" );
    Serial.println( normalizedLevel);
 */
}

//Tank is approx 160 cm: at lowest level
#define LowLevel 120 //tamk is approx 160 cm
#define MidLevel 90  
#define HighLevel 60
#define FullLevel 30

void setLevels()
{  
  isTankFilled = false; //By default set to true - just as a heck. as otherwise -at first - it detects that water flow is there and tank is empty and hence starts the motor.
  isTankLevelHigh = false;
  isTankMid = false;
  isTankLow = false;
  isTankLevelVeryLow = false;
  
  if (normalizedLevel > LowLevel) {
     isTankLevelVeryLow = true;
     //Serial.print( "Level Very Low" );
  }
  else if (normalizedLevel <=LowLevel) {
     isTankLow = true;
     
     if (normalizedLevel <=MidLevel) {
        isTankMid = true;
        if (normalizedLevel <=HighLevel) {
           isTankLevelHigh = true;
           if (normalizedLevel <=FullLevel) {
              isTankFilled = true;   
              //Serial.println( "Level Full" );
           }
           else
           {
              //Serial.print( "Level High" );    
           }
        }
        else
        {
          //Serial.print( "Level Mid" );        
        }
     }
     else
     {
        //Serial.print( "Level Low" );
     }
  }
  
  
  
  processLowLevelSensor2();
  processMidLevelSensor2();
  processHighLevelSensor2();
  processTankFullSensor2();
  
}


void processTankFullSensor2() {
   if ( tankFullState == LOW && isTankFilled==true) {
     //No change in state 
   }
   else if ( tankFullState == HIGH && isTankFilled==false) {
     //No change in state
   }
   else if ( tankFullState == LOW && isTankFilled==false) {
     //tank state changing from full to not filled.
     tankFullState = HIGH;
     save_full_level_lost_time();
     sendEventToDevice(eventId_Full_Lost_Time);
     LevelChangeTune();
     //sendStatsToDevice();
     //sendtoDevice();
   }
   else if ( tankFullState == HIGH && isTankFilled==true) {
     //tank state changing from not full to filled.
     tankFullState = LOW;
     save_full_level_detect_time();
     sendEventToDevice(eventId_Full_Detect_Time);
     LevelChangeTune();
     //sendStatsToDevice();
     //sendtoDevice();
   }
   if ( isTankFilled==true) {
        //lcd.print("Lvl#4FULL");
        digitalWrite(ledTankLow, HIGH);
        digitalWrite(ledTankMid, HIGH);
        digitalWrite(ledTankHigh, HIGH);

        //digitalWrite(ledTankFull, LOW);
        //delay(20);
        digitalWrite(ledTankFull, HIGH);          
  }
}
 
void processLowLevelSensor2() {
   if ( lowLevelState == LOW && isTankLow==true) {
     //No change in state 
   }
   else if ( lowLevelState == HIGH && isTankLow==false) {
     //No change in state
   }
   else if ( lowLevelState == LOW && isTankLow==false) {
     //tank state changing from low to very low.
     lowLevelState = HIGH;
     save_low_level_lost_time();
     sendEventToDevice(eventId_Low_Lost_Time);
     LevelChangeTune();
     //sendStatsToDevice();
     //sendtoDevice();
   }
   else if ( lowLevelState == HIGH && isTankLow==true) {
     //tank state changing from very low to low.
     lowLevelState = LOW;
     save_low_level_detect_time();
     sendEventToDevice(eventId_Low_Detect_Time);
     LevelChangeTune();
     //sendStatsToDevice();
     //sendtoDevice();
   }
   
   if (isTankLevelVeryLow == true)
   {
        digitalWrite(ledTankLow, HIGH);
        delay(30);
        digitalWrite(ledTankLow, LOW);

        //currentVeryLowDetectCount++;
        //if (currentVeryLowDetectCount >=veryLowDetectionMaxCount)
        //{
        //  veryLowLevelTune();
          //delay(100);
        //  currentVeryLowDetectCount =0;
        //}
        
        //LevelChangeTune();
        digitalWrite(ledTankMid, LOW);
        digitalWrite(ledTankHigh, LOW);
        digitalWrite(ledTankFull, LOW);
   }
   
   else if (isTankLevelVeryLow == false)
   {
        digitalWrite(ledTankLow, HIGH);
        digitalWrite(ledTankMid, LOW);
        digitalWrite(ledTankHigh, LOW);
        digitalWrite(ledTankFull, LOW);
   }
}

void processMidLevelSensor2()
{
   if ( midLevelState == LOW && isTankMid==true) {
     //No change in state 
   }
   else if ( midLevelState == HIGH && isTankMid==false) {
     //No change in state
   }
   else if ( midLevelState == LOW && isTankMid==false) {
     //tank state changing from Mid to low.
     midLevelState = HIGH;
  
     save_mid_level_lost_time();
     sendEventToDevice(eventId_Mid_Lost_Time);
                
     LevelChangeTune();
     //sendStatsToDevice();
     //sendtoDevice();
   }
   else if ( midLevelState == HIGH && isTankMid==true) {
     //tank state changing from low to mid.
     midLevelState = LOW;
     save_mid_level_detect_time();
     sendEventToDevice(eventId_Mid_Detect_Time);
     LevelChangeTune();
     //sendStatsToDevice();
     //sendtoDevice();
   }
   if (isTankMid == true)
   {
        digitalWrite(ledTankLow, HIGH);        
        digitalWrite(ledTankMid, HIGH);
        digitalWrite(ledTankHigh, LOW);
        digitalWrite(ledTankFull, LOW);
   }
}

void processHighLevelSensor2()
{
  if ( highLevelState == LOW && isTankLevelHigh==true) {
     //No change in state 
   }
   else if ( highLevelState == HIGH && isTankLevelHigh==false) {
     //No change in state
   }
   else if ( highLevelState == LOW && isTankLevelHigh==false) {
     //tank state changing from High to Mid.
     highLevelState = HIGH;

     save_high_level_lost_time();
     sendEventToDevice(eventId_High_Lost_Time);
                
     LevelChangeTune();
     //sendStatsToDevice();
     //sendtoDevice();
   }
   else if ( highLevelState == HIGH && isTankLevelHigh==true) {
     //tank state changing from mid to high.
     highLevelState = LOW;
     save_high_level_detect_time();
     sendEventToDevice(eventId_High_Detect_Time);
     LevelChangeTune();
     //sendStatsToDevice();
     //sendtoDevice();
   }
   if (isTankLevelHigh == true)
   {
        digitalWrite(ledTankLow, HIGH);        
        digitalWrite(ledTankMid, HIGH);
        digitalWrite(ledTankHigh,HIGH);
        digitalWrite(ledTankFull, LOW);
   }
}

// the loop() method runs over and over again,
// as long as the Arduino has power
int significantTimeWithoutLcdInit = 0;
int MAXITERATIONS_WITHOUT_LCDINIT = 25; //It need to be calibrated

bool isAutomatic = true;

bool onlyOnce= false;
void loop ()    
{ 

/*
   if (onlyOnce==false)
   {
     // DS3231 seconds, minutes, hours, day, date, month, year 
     //setDS3231time(second, minute, hour, day, date, month, year);
     //setDS3231time(30,36,17,1,2,6,19);
     onlyOnce = true;
            mor_start_hr = 5;
            mor_start_min = 0;
            mor_max_retry = 10;
            eve_start_hr = 21;
            eve_start_min = 0;
            eve_max_retry = 10;
            retry_interval = 10;
            MAXMINSWITHOUTWATER = 2;
            
     change_motor_profile();

   }
*/
 processFlowSensorRate();

 
 bluettothAction();
 if (motorSwitchCondition == true){
      //blink it
      digitalWrite(ledMotorOn, LOW);
      delay (20);
      digitalWrite(ledMotorOn, HIGH);      
  }
  else {
      digitalWrite(ledMotorOn, LOW);
  }
  
 
  //processType1LevelSensors();
  //Following is new level sensors detection & processing 
  processType2LevelSensors();
   
  delay(100);
  boolean turnOnMotor = false;
  turnOnMotor = isTimeToTurnMotorOnLed();
  if (turnOnMotor==true)
  { 
    //Serial.print ("time to turn on motor Automatically ");
  }
  //Serial.println (turnOnMotor, DEC);  
  processForceStartStopToggleSwitch2();
  processModeSwitch();
 
  if (isAutomatic)
  {
      //Serial.print ("test");
    if (turnOnMotor == true ) //auto mode and turnOn time
    {
         //Serial.print ("test3");
         //Serial.print ("Time to TurnOn Motor\r\n");
         turnMotorOn(MOTOR_START_REASON_AUTO_TIME);        
         //motorOnMins = 0;
         //motorStartMillis = millis();
    }
    else //motor may be running for some time or may not be running - comes here only in auto mode
    {
        processAutoStop();
    }
  }
  else //This is manual mode - take relative actions:
  {
      //Serial.print ("test2");
     processManualActions();
  }
  
  
}

void blinkManual()
{
  digitalWrite(ledSystemOn, LOW);
  delay(10);
  digitalWrite(ledSystemOn, HIGH);
}

void blinkWater()
{
  digitalWrite(ledWaterFlow, LOW);
  delay(10);
  digitalWrite(ledWaterFlow, HIGH);
}


void blinkMotor()
{
  digitalWrite(ledMotorOn, LOW);
  delay(10);
  digitalWrite(ledMotorOn, HIGH);
}

void blinkTankFull()
{
  digitalWrite(ledTankFull, LOW);
  delay(10);
  digitalWrite(ledTankFull, HIGH);
}

void blinkManualAndWater()
{
  digitalWrite(ledWaterFlow, LOW);
  digitalWrite(ledSystemOn, LOW);
  delay(20);
  digitalWrite(ledSystemOn, HIGH);
  digitalWrite(ledWaterFlow, HIGH);
}

void blinkManualAndMotor()
{
  digitalWrite(ledSystemOn, LOW);
  digitalWrite(ledMotorOn, LOW);
  delay(20);
  digitalWrite(ledSystemOn, HIGH);
  digitalWrite(ledMotorOn, HIGH);
}
void processManualActions()
{
      //manual mode specific condidtions for lights blinking
      if (motorSwitchCondition == true)    
      {
         //if water is gone - blink -  manual & motor
         if (isWaterInFlow == false)
         {
            motorOnWithoutWaterMins = (millis() - waterGoneTimeMillis) / 60000;
            significantTimeMotorRunningWithoutWater++;
            if (motorOnWithoutWaterMins >= MAXMINSWITHOUTWATER)
            {
               LevelChangeTune();
               blinkManualAndMotor();
            }
         }
         else
         {
            motorOnWithoutWaterMins = 0;
         }
   
         if (isTankFilled ==true)
         //if tank is full - blink - manual, motor, full
         {
              LevelChangeTune();
              blinkManualAndMotor();
              blinkTankFull();
         }   
      }
      else
      {
         //water tank empty and water in-flow there::
         //beep manual and water-inflow, and level led
         if (isTankFilled==false )
         {
            if (isTankLevelHigh==false )
            {
               if (isWaterInFlow == true)
               {
                 LevelChangeTune();
                 blinkManualAndWater();
               }
            }
         }
      }
}

void processAutoStop()
{
       if ( isTankFilled == true)
       {
           //Serial.print ("Water Tank is Full - Turn off motor immediately");
           turnMotorOff(MOTOR_STOP_REASON_TANK_FULL); 
           //motorOnMins = 0;
           //motorStartMillis = millis();
       }
       //If motor is running - with significant time- stop it.
       //else if motor is not running - water is not there -- do nothing.
        //If motor is running - we might need to switch it off:
       else if (motorSwitchCondition == true) 
       {
          /*
          //motorOnMins = (millis() - motorStartMillis)/60000;
          if (motorOnReason == MOTOR_START_REASON_APP_TRIGGER)
          {
             //do not ignore waterflow, if motor was turned on from app
             //Now it can be turned off - with three reasons::
             //1> Water tank is full
             //2> turn off from app
             //3> power off
             //4> manual trigger
             //No - Water
          }
          else */
          if (isWaterInFlow == false)
          {
              motorOnWithoutWaterMins = (millis() - waterGoneTimeMillis) / 60000;
              significantTimeMotorRunningWithoutWater++;
              if (motorOnWithoutWaterMins >= MAXMINSWITHOUTWATER)
              {
                  turnMotorOff(MOTOR_STOP_REASON_WATER_FLOW_LOST);
                  motorOnWithoutWaterMins = 0;
                  waterGoneTimeMillis = millis();
              }
          }
          else
          {
            motorOnWithoutWaterMins = 0;
          }
       }
}

void processForceStartStopToggleSwitch2()
{  
  //if detected high - anytime - then toggle the current condition:
  // Means - if high - if motor was on - turn it off.
                      //if motor was off - turn it on.

  //Read force motor start button:
  reading1 = digitalRead(forceMotorStartSignal);
  if (reading1 == LOW) //Toggle was initiated - just now
  {
        //Serial.print ("Toggle button high " );
        //if motor off 
        if (motorSwitchCondition == false)
        {
          //Serial.print ("Motor off-->on " );
           //digitalWrite(ledSystemOn, HIGH);
           turnMotorOn(MOTOR_START_REASON_MANUAL_TRIGGER);
        }
        else
        {
           //Serial.print ("Motor On-->off " );
           //digitalWrite(ledSystemOn, LOW);
           turnMotorOff(MOTOR_STOP_REASON_MANUAL_TRIGGER);    
        }
        //sendtoDevice();
  }  
  else //now it is disconnected.
  {
    //if motor was switched off due to some reason - then we shall reset the manual toggle as well.
    if (motorSwitchCondition == false)
    {
      //digitalWrite(ledSystemOn, LOW);
    }
  }
}
  
//Low - means manual
//high - means automatic
void processModeSwitch ()
{
  //Read force motor start button:
  reading1 = digitalRead(motoMode);
  
  //do following to play tone on pressing the switch
  if (reading1==manualModeState)
  {
     
  }
  else //switch just triggered
  {

      //play sound here:
      LevelChangeTune();
      // here- take action :
      // Auto--> manual mode switch  -- if motor was on - turn it off
      if (reading1 == LOW)
      {
          save_mode_manual_time();
          sendEventToDevice(eventId_Mode_Manual_Time);
          //Serial.println("Turning motor Off");
          turnMotorOff(MOTOR_STOP_REASON_MANUAL_TRIGGER);  
      }
      else
      {
        save_mode_auto_time();
        sendEventToDevice(eventId_Mode_Auto_Time);
      }
      manualModeState = reading1; 

  }

  //process according to switch condition
  if (reading1 == LOW)
  {
    digitalWrite(ledSystemOn, HIGH);
    isAutomatic = false;
  }
  else
  {
    digitalWrite(ledSystemOn, LOW);
    isAutomatic = true;
  }
  
}


void processFlowSensorRate()
{
  
  if((millis() - oldTime) > 1000)    // Only process counters once per second
  {
    
    // Disable the interrupt while calculating flow rate and sending the value to
    // the host
    detachInterrupt(sensorInterrupt);

    flowReadings[flowReadingiteration] = pulseCount; //pulsecount is pulse count in that particular second
    flowReadingCount = flowReadingCount + pulseCount; //add the current reading
    //minus the reading which is current - last maxth reading... so to have sum of only last max-1 enteries.... 
    // It means, we have to minus the next to the current reading...so as to have sum of last max-1 enteries
    if (flowReadingiteration >= MAXFlowReadings-1)
    {
      flowReadingiteration=0;
    }
    else
    {
      flowReadingiteration++;
    }
    flowReadingCount = flowReadingCount - flowReadings[flowReadingiteration];    
    //Serial.print("Pulse Count: ");
    //Serial.print(pulseCount);
    //Serial.print(" -- ");
    //Serial.println(flowReadingCount);

    if(flowReadingCount > 0)
    {
      //Consider this as "Pani Aa raha hai"
      newWaterFlowState = LOW;
    }
    else
    {
      //Consider this as "Pani nahi aa raha hai"
      newWaterFlowState = HIGH;
    }
    if (newWaterFlowState ==   waterInFlowState) //No change in state - same as last one
    {
       //lastDebounceTimeFlow = 0;
       if (waterInFlowState == LOW) //connected
       {
          //Serial.print ("Paani aa raha hai");
          isWaterInFlow = true;
       }
       else //not connected
       {
          //Serial.print ("Paani nahi aa raha hai ");
          isWaterInFlow = false;  
       }
    }
    else
    {
          if (waterInFlowState == LOW) //It means previously water was on - now it is gone.
          {
            waterGoneTimeMillis = millis();
            motorOnWithoutWaterMins = 0;
            //Serial.print ("Paani just abhee chala gaya ");
            isWaterInFlow = false;
            waterInFlowState = HIGH;
            digitalWrite(ledWaterFlow, LOW);
            save_water_lost_time();
            sendEventToDevice(eventId_Water_Lost_Time);
          }
          // only toggle the LED if the new button state is HIGH
          else  
          {
            //Serial.print ("Paani just abhee aa gaya");
            isWaterInFlow = true;
            waterInFlowState = LOW;
            digitalWrite(ledWaterFlow, HIGH);
            save_water_detect_time();
            sendEventToDevice(eventId_Water_Detect_Time);
          }
          LevelChangeTune();
    }
    
    
    // Because this loop may not complete in exactly 1 second intervals we calculate
    // the number of milliseconds that have passed since the last execution and use
    // that to scale the output. We also apply the calibrationFactor to scale the output
    // based on the number of pulses per second per units of measure (litres/minute in
    // this case) coming from the sensor.
    flowRate = ((1000.0 / (millis() - oldTime)) * pulseCount) / calibrationFactor;
    
    
    // Note the time this processing pass was executed. Note that because we've
    // disabled interrupts the millis() function won't actually be incrementing right
    // at this point, but it will still return the value it was set to just before
    // interrupts went away.
    oldTime = millis();
    
    // Divide the flow rate in litres/minute by 60 to determine how many litres have
    // passed through the sensor in this 1 second interval, then multiply by 1000 to
    // convert to millilitres.
    flowMilliLitres = (flowRate / 60) * 1000;
    
    // Add the millilitres passed in this second to the cumulative total
    totalMilliLitres += flowMilliLitres;
      
    unsigned int frac;
    /*
    // Print the flow rate for this second in litres / minute
    Serial.print("Flow rate: ");
    Serial.print(int(flowRate));  // Print the integer part of the variable
    Serial.print("L/min");
    Serial.print("\t");       // Print tab space

    // Print the cumulative total of litres flowed since starting
    Serial.print("Output Liquid Quantity: ");        
    Serial.print(totalMilliLitres);
    Serial.println("mL"); 
    Serial.print("\t");       // Print tab space
    Serial.print(totalMilliLitres/1000);
    Serial.print("L");
    */

    // Reset the pulse counter so we can start incrementing again
    pulseCount = 0;
    
    // Enable the interrupt again now that we've finished sending output
    attachInterrupt(sensorInterrupt, pulseCounter, FALLING);
  } 

   if (isWaterInFlow == true)
   {
      //blink it
      //digitalWrite(ledWaterFlow, LOW);
      //delay (20);
      digitalWrite(ledWaterFlow, HIGH);
      //Serial.print("WaterFlow Detected\r\n");
      
   }
   else
   {
      digitalWrite(ledWaterFlow, LOW);
      //Serial.print("WaterFlow Lost\r\n");

   }
}

/*
Insterrupt Service Routine
 */
void pulseCounter()
{
  // Increment the pulse counter
  pulseCount++;
}


void processFlowSensor ()
{
  //processFlowSensorRate();
  
  reading2 = digitalRead(hallsensor);
  //Serial.print ("Tank hallsensor status ");
  //Serial.print (reading2, DEC); 
  //Serial.print ("  done \r\n");
   if (reading2 ==   waterInFlowState) //No change in state - same as last one
   {
       lastDebounceTimeFlow = 0;
       if (waterInFlowState == LOW) //connected
       {
          //Serial.print ("Paani aa gaya");
          isWaterInFlow = true;
       }
       else //not connected
       {
          //Serial.print ("Paani chala gaya ");
          isWaterInFlow = false;  
       }
   }
   else //state change in process
   {
       if (lastDebounceTimeFlow == 0 ) //Just changed
       {
          lastDebounceTimeFlow = millis();
       }
       else if ((millis() - lastDebounceTimeFlow) > debounceDelayFlow) // significant time change triggered
       {
           waterInFlowState = reading2;
          //take action and change state
          //if it is changing then reset waterGoneTime:
          if ((isWaterInFlow == true) && (waterInFlowState == HIGH)) //It means previously water was on - now it is gone.
          {
            waterGoneTimeMillis = millis();
            motorOnWithoutWaterMins = 0;
            //Serial.print ("Paani just abhee chala gaya ");
            isWaterInFlow = false;
            //Serial.print("\r\nwaterGoneTimeMillis############");
            //Serial.print (waterGoneTimeMillis, DEC);
            //Serial.print("\r\n");
          }
          // only toggle the LED if the new button state is HIGH
          if (waterInFlowState == LOW) 
          {
            //Serial.print ("Paani aa gaya");
            isWaterInFlow = true;
            save_water_detect_time();
            sendEventToDevice(eventId_Water_Detect_Time);
          }
          else
          {
            //Serial.print ("Paani chala gaya ");
            isWaterInFlow = false;
            save_water_lost_time();
            sendEventToDevice(eventId_Water_Lost_Time);
          }
          //sendStatsToDevice();
          //sendtoDevice();
          LevelChangeTune();   
       }
       else
       {
          if (waterInFlowState == LOW) 
          {
            isWaterInFlow = true;
          }
          else
          {
            isWaterInFlow = false;
          }
       }
   }
   if (isWaterInFlow == true)
   {
      //blink it
      //digitalWrite(ledWaterFlow, LOW);
      //delay (20);
      digitalWrite(ledWaterFlow, HIGH);
      //Serial.print("WaterFlow Detected\r\n");
      
   }
   else
   {
      digitalWrite(ledWaterFlow, LOW);
      //Serial.print("WaterFlow Lost\r\n");

   }
}

void processType1LevelSensors()
{
  processLowLevelSensor();
  processMidLevelSensor();
  processHighLevelSensor();
  processTankFullSensor();
}

void processTankFullSensor()
{
   reading3 = digitalRead(tankFullSensor);
   if (reading3 == tankFullState) // No change in state
   {
      lastDebounceTimeTank = 0;
      if (tankFullState == LOW) 
      {
          //Serial.print ("Tank Full - happy ");
          isTankFilled = true;
      }
      else
      {
          //Serial.print ("Tank Not Full - :( ");
          isTankFilled = false;
      }
   }
   else //change in state
   {
       if ( lastDebounceTimeTank ==0 )// Just changed the state
       {
          lastDebounceTimeTank = millis();
       }
       else if ( (millis() - lastDebounceTimeTank) > debounceDelayTank)
       {
         tankFullState = reading3;
         //Take action and change the state
         if (tankFullState == LOW) 
         {
            //Serial.print ("Tank Full - happy ");
            isTankFilled = true;
            save_full_level_detect_time();
            sendEventToDevice(eventId_Full_Detect_Time);
            
         }
         else
         {
            //Serial.print ("Tank Not Full - :( ");
            isTankFilled = false;
            save_full_level_lost_time();
            sendEventToDevice(eventId_Full_Lost_Time);
         }
         LevelChangeTune();

         //sendStatsToDevice();
         //sendtoDevice();
       }
       else
       {
         if (tankFullState == LOW) 
         {
            //Serial.print ("Tank Full - happy ");
            isTankFilled = true;
         }
         else
         {
            isTankFilled = false;
         }
        
       }
   }
   if ( isTankFilled==true)
   {
        //lcd.print("Lvl#4FULL");
        digitalWrite(ledTankLow, HIGH);
        digitalWrite(ledTankMid, HIGH);
        digitalWrite(ledTankHigh, HIGH);

        //digitalWrite(ledTankFull, LOW);
        //delay(20);
        digitalWrite(ledTankFull, HIGH);          
  }
}

int veryLowDetectionMaxCount = 2000;
int currentVeryLowDetectCount = 0; 
void processLowLevelSensor()
{
   reading4 = digitalRead(lowLevelSensor);  
   if (reading4 == lowLevelState) 
   {
      lastDebounceTimeLevel1 = 0;
      if (lowLevelState == LOW) // means it is connected
      {
          isTankLevelVeryLow = false;
          isTankLow = true;
      }
      else // not connected
      {
          isTankLevelVeryLow = true;
          isTankLow = false;
      }
   }
   else //change in process
   {
      if (lastDebounceTimeLevel1 == 0) //Change just trigeering
      {
         lastDebounceTimeLevel1 = millis();
      }
      else if ((millis() - lastDebounceTimeLevel1) > debounceDelayTankLevel1)//change just completed for significant time
      {
        lowLevelState = reading4;
        if (lowLevelState == LOW) // means it is connected
        {
           isTankLevelVeryLow = false;
           isTankLow = true;
           save_low_level_detect_time();
           sendEventToDevice(eventId_Low_Detect_Time);
        }
        else // not connected
        {
           isTankLevelVeryLow = true;
           isTankLow = false;
           save_low_level_lost_time();
           sendEventToDevice(eventId_Low_Lost_Time);
        }
        
        LevelChangeTune();
        //sendtoDevice();    
      }
      else //less than significant time for change
      {
        if (lowLevelState == LOW) // means it is connected
        {
           isTankLevelVeryLow = false;
           isTankLow = true;
        }
        else // not connected
        {
           isTankLevelVeryLow = true;
           isTankLow = false;
        }
      }
   }
   if (isTankLevelVeryLow == true)
   {
        currentVeryLowDetectCount++;
        digitalWrite(ledTankLow, LOW);
        if (currentVeryLowDetectCount >=veryLowDetectionMaxCount)
        {
           veryLowLevelTune();
          //delay(100);
          digitalWrite(ledTankLow, HIGH);
          currentVeryLowDetectCount =0;
        }
        //LevelChangeTune();
        digitalWrite(ledTankMid, LOW);
        digitalWrite(ledTankHigh, LOW);
        digitalWrite(ledTankFull, LOW);
   }
   else
   {
        digitalWrite(ledTankLow, HIGH);
        digitalWrite(ledTankMid, LOW);
        digitalWrite(ledTankHigh, LOW);
        digitalWrite(ledTankFull, LOW);
   }
}


void processMidLevelSensor()
{
   reading4 = digitalRead(midLevelSensor);
   if (reading4 == midLevelState) 
   {
      lastDebounceTimeLevel2 = 0;
      if (midLevelState == LOW) // means it is connected
      {
          isTankLevelVeryLow = false;
          isTankMid = true;
      }
      else // not connected
      {
          //isTankLevelVeryLow = true;
          isTankMid = false;
      }
   }
   else //change in process
   {
      if (lastDebounceTimeLevel2 == 0) //Change just trigeering
      {
         lastDebounceTimeLevel2 = millis();
      }
      else if ((millis() - lastDebounceTimeLevel2) > debounceDelayTankLevel2)//change just completed for significant time
      {
        midLevelState = reading4;
        //Take action and change state
        if (midLevelState == LOW) // means it is connected
        {
           isTankLevelVeryLow = false;
           isTankMid = true;
           save_mid_level_detect_time();
           sendEventToDevice(eventId_Mid_Detect_Time);
        }
        else // not connected
        {
           //isTankLevelVeryLow = true;
           isTankMid = false;
           save_mid_level_lost_time();
           sendEventToDevice(eventId_Mid_Lost_Time);
        }
        LevelChangeTune();
        //sendtoDevice();    
      }
      else //less than significant time for change
      {
        if (midLevelState == LOW) // means it is connected
        {
           isTankLevelVeryLow = false;
           isTankMid = true;
        }
        else // not connected
        {
           //isTankLevelVeryLow = true;
           isTankMid = false;
        }
      }
   }
   
   if (isTankMid == true)
   {
        digitalWrite(ledTankLow, HIGH);        
        digitalWrite(ledTankMid, HIGH);
        digitalWrite(ledTankHigh, LOW);
        digitalWrite(ledTankFull, LOW);
   }
}

/*
void processHighLevelSensorTest()
{
  reading5 = digitalRead(highLevelSensor);
  if (reading5 == highLevelState) 
  {
      lastDebounceTimeLevel3 = 0;
      if (highLevelState == HIGH)
      {
          //printSerial ("Tank level low  :(" );
          isTankLevelHigh = false;
          //isTankLevelLow = false;
          
      }
      else
      {
          //printSerial ("Tank level high  :) ");
          isTankLevelHigh = true;
          isTankLevelVeryLow = false;
      }
  }
  else //change in progress
  {
      if ( lastDebounceTimeLevel3==0) //change just started
      {
        lastDebounceTimeLevel3 = millis();
      }
      else if ((millis() - lastDebounceTimeLevel3) > debounceDelayTankLevel3) // change is 
      {
         //time to take action and change state
         highLevelState = reading5;
         if (highLevelState == LOW)
         {
            isTankLevelHigh = true;
            isTankLevelVeryLow = false;
         }
         else
         {
           isTankLevelHigh = false;
         }
      }
      else
      {
         if (highLevelState == LOW)
         {
            isTankLevelVeryLow = false;
         }
         else
         {
           isTankLevelHigh = false;
         }
      }
  }
  if (isTankLevelHigh == true)
  { 
    digitalWrite(ledTankLow, HIGH);
    digitalWrite(ledTankMid, HIGH);
    //digitalWrite(ledTankHigh, LOW);
    //delay(20);
    digitalWrite(ledTankHigh, HIGH);
    digitalWrite(ledTankFull, LOW);
    //delay (20);
  }
}
*/

void processHighLevelSensor()
{
  reading5 = digitalRead(highLevelSensor);
  if (reading5 == highLevelState) 
  {
      lastDebounceTimeLevel3 = 0;
      if (highLevelState == HIGH)
      {
          //printSerial ("Tank level low  :(" );
          isTankLevelHigh = false;
          //isTankLevelLow = false;
      }
      else
      {
          //printSerial ("Tank level high  :) ");
          isTankLevelHigh = true;
          isTankLevelVeryLow = false;
      }
  }
  else //change in progress
  {
      if ( lastDebounceTimeLevel3==0) //change just started
      {
        lastDebounceTimeLevel3 = millis();
      }
      else if ((millis() - lastDebounceTimeLevel3) > debounceDelayTankLevel3) // change is 
      {
         //time to take action and change state
         highLevelState = reading5;
         if (highLevelState == LOW)
         {
            //printSerial ("Tank level good high Changed - :)" );
            isTankLevelHigh = true;
            //isTankLevelLow = false;
            isTankLevelVeryLow = false;
            save_high_level_detect_time();
            sendEventToDevice(eventId_High_Detect_Time);
         }
         else
         {
           //printSerial ("Tank less than high changed- :( ");
           isTankLevelHigh = false;
           save_high_level_lost_time();
           sendEventToDevice(eventId_High_Lost_Time);
         }
         //sendStatsToDevice();
         //sendtoDevice();
         LevelChangeTune();
      }
      else
      {
         if (highLevelState == LOW)
         {
            //printSerial ("Tank level good high Changed - :)" );
            isTankLevelHigh = true;
            //isTankLevelLow = false;
            isTankLevelVeryLow = false;
         }
         else
         {
           isTankLevelHigh = false;
         }
      }
  }
  if (isTankLevelHigh == true)
  { 
    digitalWrite(ledTankLow, HIGH);
    digitalWrite(ledTankMid, HIGH);
    //digitalWrite(ledTankHigh, LOW);
    //delay(20);
    digitalWrite(ledTankHigh, HIGH);
    digitalWrite(ledTankFull, LOW);
    //delay (20);
  }
}

void printSerial (char * string)
{
  Serial.println (string);
}
